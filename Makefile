.PHONY: serve_http
serve_http:
	air

.PHONY: test_all_v
test_all_v:
	go clean -testcache && go test -v ./...

.PHONY: test_all
test_all:
	go clean -testcache && go test ./...

.PHONY: gen_openapi
gen_openapi:
	sh scripts/gen_openapi.sh

.PHONY: gen_mocks
gen_mocks:
	sh scripts/gen_mocks.sh
