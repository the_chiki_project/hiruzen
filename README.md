# Hiruzen

Calculate Z score to track children's growth.

**Hiruzen** is the complement for [Asuma](https://github.com/theobitoproject/asuma) project

<img src="hiruzen.jpeg" alt="Hiruzen's image" width="320"/>

> To those who possess the **Will of Fire**, everyone is family. The desire to protect one's family builds thicker and stronger bonds between each and everyone in the village. If the **Will of Fire** is embraced by everyone, the village will be alright no matter what happens.

**Hiruzen Sarutobi**, Third Hokage, hailed as a "God of Shinobi" and sensei of the Sannin.

## Local development

### Setup

#### 1. Set environment variables

Create a `.env` file in the root of the project. Copy all content from `env.example` into `.env`. No more changes should be needed.

**Note:** You can modify what you consider is necessary.

#### 2. Expose services

From withing the container, run:

```sh
make serve_http
```

### Testing

You could use any command from the Go testing framework. Still, there's a command that runs all tests without cache.

```sh
make test_all
```

### API changes

Any API changes needs to be reflected on swagger definitions located in [api/openapi](https://github.com/theobitoproject/hiruzen/tree/main/api/openapi/hiruzen.yml) directory. After modifying these files, generate http boilerplate with:

```sh
make gen_openapi
```

### Go dependencies

Any new package added or any package that has been updated/upgraded, needs to be included in the vendor directory. So, don't forget to run these two commands:

```sh
go mod tidy
go mod vendor
```

## Pending work

- Start using [Ginkgo testing library](https://github.com/onsi/ginkgo).
- Create a proper data store for the [filename repository information](https://github.com/theobitoproject/hiruzen/blob/main/support/zscore_standard_curves/filename_repo.csv).
- Relocate [Excel data files](https://github.com/theobitoproject/hiruzen/tree/main/support/zscore_standard_curves) to a proper storage. These files shouldn't be included in the repo.
- Create a CRUD for storing children information and this way track Z score in time.
