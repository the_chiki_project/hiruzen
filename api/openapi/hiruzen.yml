openapi: '3.0.2'
info:
  title: Hiruzen API
  description: >
    Hiruzen API takes data from a kid (height, weight, age, etc) and
    calculates her/his growth Z score
  version: '1.0.0'

servers:
  - url: http://localhost:3000/
  
paths:
  /zscore/standard/import:
    post:
      summary: imports all standard data into database
      operationId: importAllStandards
      responses:
        '204':
          $ref: '#/components/responses/NoContent'
        '400':
          $ref: '#/components/responses/BadRequest'
        '500':
          $ref: '#/components/responses/InternalServerError'

  /zscore/standard/import-one:
    post:
      summary: imports data for a single standard into database
      operationId: importSingleStandard
      parameters:
        - $ref: '#/components/parameters/StandardNameQuery'
        - $ref: '#/components/parameters/GenderQuery'
      responses:
        '204':
          $ref: '#/components/responses/NoContent'
        '400':
          $ref: '#/components/responses/BadRequest'
        '500':
          $ref: '#/components/responses/InternalServerError'

  /zscore/standard/data-point-ranges:
    get:
      summary: returns the data point ranges for the specified standard
      operationId: getDataPointRanges
      parameters:
        - $ref: '#/components/parameters/StandardNameQuery'
        - $ref: '#/components/parameters/GenderQuery'
      responses:
        '200':
          $ref: '#/components/responses/GetDataPointRangesResponse'
        '400':
          $ref: '#/components/responses/BadRequest'
        '500':
          $ref: '#/components/responses/InternalServerError'

  /zscore:
    get:
      summary: computes and returns a custom Z score
      operationId: getZScore
      parameters:
        - $ref: '#/components/parameters/DateOfBirthQuery'
        - $ref: '#/components/parameters/DateOfVisitQuery'
        - $ref: '#/components/parameters/GenderQuery'
        - $ref: '#/components/parameters/MeasuredTypeQuery'
        - $ref: '#/components/parameters/HeightQuery'
        - $ref: '#/components/parameters/WeightQuery'
      responses:
        '200':
          $ref: '#/components/responses/GetZScoreResponse'
        '400':
          $ref: '#/components/responses/BadRequest'
        '500':
          $ref: '#/components/responses/InternalServerError'

components:

  responses:

    GetDataPointRangesResponse:
      description: list of data point ranges
      content:
        application/json:
          schema:
            type: object
            required:
              - dataPointReferenceName
              - dataPointRanges
            properties:
              dataPointReferenceName:
                $ref: '#/components/schemas/DataPointReferenceName'
              dataPointRanges:
                $ref: '#/components/schemas/DataPointRanges'

    GetZScoreResponse:
      description: Z score values for all standards
      content:
        application/json:
          schema:
            type: object
            required:
              - BMI
              - age
              - zScores
            properties:
              BMI:
                $ref: '#/components/schemas/BMI'
              age:
                $ref: '#/components/schemas/Age'
              zScores:
                $ref: '#/components/schemas/ZScoreResults'
      
    BadRequest:
      description: Bad request
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    InternalServerError:
      description: Unexpected error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    NoContent:
      description: successful response with no content
                
  parameters:

    StandardNameQuery:
      in: query
      name: standardName
      required: true
      schema:
        $ref: '#/components/schemas/StandardName'

    GenderQuery:
      in: query
      name: gender
      required: true
      schema:
        $ref: '#/components/schemas/Gender'

    DateOfBirthQuery:
      in: query
      name: dateOfBirth
      required: true
      schema:
        $ref: '#/components/schemas/DateOfBirth'
    
    DateOfVisitQuery:
      in: query
      name: dateOfVisit
      required: true
      schema:
        $ref: '#/components/schemas/DateOfVisit'
    
    MeasuredTypeQuery:
      in: query
      name: measuredType
      required: true
      schema:
        $ref: '#/components/schemas/MeasuredType'
    
    HeightQuery:
      in: query
      name: height
      required: true
      schema:
        $ref: '#/components/schemas/Height'
    
    WeightQuery:
      in: query
      name: weight
      required: true
      schema:
        $ref: '#/components/schemas/Weight'

  schemas:

    DateOfBirth:
      type: string
      format: date

    Gender:
      type: string
      enum:
        - boy
        - girl

    DateOfVisit:
      type: string
      format: date-time
    
    MeasuredType:
      type: string
      enum:
        - recumbent
        - standing
    
    Height:
      type: number
      minimum: 0
    
    Weight:
      type: number
      minimum: 0

    BMI:
      type: number
      minimum: 0

    ZScore:
      type: object
      required:
        - ageRange
        - reference
        - value
      properties:
        ageRange:
          $ref: '#/components/schemas/AgeRange'
        reference:
          type: number
        value:
          type: number

    ZScoreResults:
      type: object
      required:
        - bmiForAge
        - lengthForAge
        - weightForAge
        - weightForLength
      properties:
        bmiForAge:
          $ref: '#/components/schemas/ZScore'
        lengthForAge:
          $ref: '#/components/schemas/ZScore'
        weightForAge:
          $ref: '#/components/schemas/ZScore'
        weightForLength:
          $ref: '#/components/schemas/ZScore'

    StandardName:
      type: string
      enum:
        - bmiForAge
        - lengthForAge
        - weightForAge
        - weightForLength

    DataPointReferenceName:
      type: string
      enum:
        - age
        - length
    
    Age:
      type: number
      minimum: 0

    AgeRange:
      type: string
      enum:
        - 0_to_2
        - 2_to_5
        - 0_to_5

    DataPointRange:
      type: object
      required:
        - ageRange
        - dataPoints
      properties:
        ageRange:
          $ref: '#/components/schemas/AgeRange'
        dataPoints:
          $ref: '#/components/schemas/DataPoints'

    DataPointRanges:
      type: array
      items:
        $ref: '#/components/schemas/DataPointRange'

    DataPoint:
      type: object
      required:
        - reference
        - sd3Neg
        - sd2Neg
        - sd1Neg
        - sd0
        - sd1Pos
        - sd2Pos
        - sd3Pos
      properties:
        reference:
          type: number
          format: float
        boxCoxPower:
          type: number
          format: float
        median:
          type: number
          format: float
        coefficientOfVariation:
          type: number
          format: float
        sd3Neg:
          type: number
          format: float
        sd2Neg:
          type: number
          format: float
        sd1Neg:
          type: number
          format: float
        sd0:
          type: number
          format: float
        sd1Pos:
          type: number
          format: float
        sd2Pos:
          type: number
          format: float
        sd3Pos:
          type: number
          format: float

    DataPoints:
      type: array
      items:
        $ref: '#/components/schemas/DataPoint'

    Error:
      type: object
      required:
        - message
      properties:
        message:
          type: string
