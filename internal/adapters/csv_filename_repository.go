package adapters

import (
	"fmt"

	"github.com/theobitoproject/myoboku/csv"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/stat"
)

// CsvFileRepository is the handler to interact
// with the standard filename data store in Mongo
// This repository holds names, paths and sheet names about the data files
type CsvFileRepository struct {
	path   string
	reader csv.CsvReader
}

// NewCsvFileRepository creates a new instance of CsvFileRepository
func NewCsvFileRepository(
	path string,
	reader csv.CsvReader,
) (CsvFileRepository, error) {
	if path == "" {
		return CsvFileRepository{}, fmt.Errorf("path cannot be empty")
	}
	if reader == nil {
		return CsvFileRepository{}, fmt.Errorf("reader cannot be empty")
	}

	return CsvFileRepository{
		path,
		reader,
	}, nil
}

// GetDataFiles returns all data files related to a single standard
func (sfr CsvFileRepository) GetDataFiles(
	standard model.Standard,
) (dfs []stat.DataFile, err error) {
	if standard.IsZero() {
		return dfs, fmt.Errorf("standard cannot be empty")
	}

	err = sfr.reader.ForEachRow(sfr.path, func(row []string) (bool, error) {
		if row[0] == standard.Gender().String() && row[1] == standard.Name().String() {
			df, err := stat.NewDataFile(row[2], row[3], row[4], row[5])
			if err != nil {
				return true, err
			}
			dfs = append(dfs, df)
			return true, nil
		}
		return true, nil
	})

	if err != nil {
		return []stat.DataFile{}, err
	}

	return dfs, err
}
