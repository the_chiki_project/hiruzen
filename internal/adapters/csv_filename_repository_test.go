package adapters

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/myoboku/csv"
	"github.com/theobitoproject/myoboku/csv/mocks"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func TestCsvFileRepository_NewCsvFileRepository(t *testing.T) {
	t.Parallel()

	testPath := "some path"

	testCases := []struct {
		name     string
		fp       CsvFileRepository
		hasError bool
	}{
		{
			name: "when reader is empty",
			fp: CsvFileRepository{
				path:   testPath,
				reader: nil,
			},
			hasError: true,
		},
		{
			name: "when file DB path is empty",
			fp: CsvFileRepository{
				path:   "",
				reader: getTestCsvReader(t, "", nil),
			},
			hasError: true,
		},
		{
			name: "when all parameter are valid",
			fp: CsvFileRepository{
				path:   testPath,
				reader: getTestCsvReader(t, testPath, nil),
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			csvFileRepository, err := NewCsvFileRepository(tcd.fp.path, tcd.fp.reader)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, csvFileRepository, tcd.fp)
			}
		})
	}
}

func TestCsvFileRepository_GetDataFile(t *testing.T) {
	t.Parallel()

	testError := fmt.Errorf("some error")
	testFileFBPath := "some path"
	testStandard := getTestStandard(t)

	testCases := []struct {
		name     string
		reader   csv.CsvReader
		path     string
		standard model.Standard
		hasError bool
	}{
		{
			name:     "when standard is empty",
			reader:   getTestCsvReader(t, testFileFBPath, nil),
			path:     testFileFBPath,
			standard: model.Standard{},
			hasError: true,
		},
		{
			name:     "when reader for each row fails",
			reader:   getTestCsvReader(t, testFileFBPath, testError),
			path:     testFileFBPath,
			standard: testStandard,
			hasError: true,
		},
		{
			name:     "when standard is NOT empty",
			reader:   getTestCsvReader(t, testFileFBPath, nil),
			path:     testFileFBPath,
			standard: getTestStandard(t),
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			fp, err := NewCsvFileRepository(tcd.path, tcd.reader)

			require.NoError(t, err)

			// TODO: assert the returned data file value
			_, err = fp.GetDataFiles(tcd.standard)

			if tcd.hasError {
				require.Error(t, err)

			} else {
				require.NoError(t, err)
			}
		})
	}
}

func getTestCsvReader(
	t *testing.T,
	testPath string,
	err error,
) *mocks.MockCsvReader {
	ctrl := gomock.NewController(t)

	tr := mocks.NewMockCsvReader(ctrl)
	tr.
		EXPECT().
		ForEachRow(testPath, gomock.Any()).
		Return(err).
		AnyTimes()

	return tr
}
