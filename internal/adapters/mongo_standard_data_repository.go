package adapters

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/theobitoproject/myoboku/mongo"
	"github.com/theobitoproject/myoboku/query"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

// MongoDataRepository is the handler to interact
// with the standard data store in Mongo
type MongoDataRepository struct {
	mongoClient mongo.MongoClient
}

// NewMongoDataRepository creates a new instance of MongoDataRepository
func NewMongoDataRepository(
	mongoClient mongo.MongoClient,
) (MongoDataRepository, error) {
	if mongoClient == nil {
		return MongoDataRepository{}, fmt.Errorf("mongo DB cannot be empty")
	}

	return MongoDataRepository{
		mongoClient,
	}, nil
}

// GetDataPointRanges returns the data for a single standard
func (dr MongoDataRepository) GetDataPointRanges(
	ctx context.Context,
	standard model.Standard,
) ([]model.DataPointRange, error) {
	if standard.IsZero() {
		return []model.DataPointRange{}, fmt.Errorf("standard cannot be empty")
	}

	qb := query.NewDefaultQueryBuilder()
	qb.Where("name", standard.Name().String())
	qb.Where("gender", standard.Gender().String())
	qb.WithFields("data")

	var mongoStandard MongoStandardModel
	err := dr.mongoClient.FindOne(ctx, qb, &mongoStandard)
	if err != nil {
		return []model.DataPointRange{}, err
	}

	return unmarshallDataPointRanges(mongoStandard.DataPointRanges)
}

// StoreStandard stores a standard together with their data and references
func (dr MongoDataRepository) StoreStandard(
	ctx context.Context,
	standard model.Standard,
	dataPointRanges []model.DataPointRange,
) error {
	if standard.IsZero() {
		return fmt.Errorf("standard cannot be empty")
	}

	filter := bson.M{
		"name":   standard.Name().String(),
		"gender": standard.Gender().String(),
	}

	err := dr.mongoClient.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	mongoStandard := MongoStandardModel{
		Name:                   standard.Name().String(),
		Gender:                 standard.Gender().String(),
		DataPointReferenceName: standard.Name().Reference(),
		DataPointRanges:        marshallDataPointRanges(dataPointRanges),
	}

	_, err = dr.mongoClient.InsertOne(ctx, mongoStandard)
	return err
}
