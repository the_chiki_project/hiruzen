package adapters

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/myoboku/mongo"
	"github.com/theobitoproject/myoboku/mongo/mocks"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func TestMongoDataRepository_NewMongoDataRepository(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		mr       MongoDataRepository
		hasError bool
	}{
		{
			name: "when mongo DB is empty",
			mr: MongoDataRepository{
				mongoClient: nil,
			},
			hasError: true,
		},
		{
			name: "when all parameters are present",
			mr: MongoDataRepository{
				mongoClient: getTestMongoClient(t, nil, nil, nil, nil),
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			mr, err := NewMongoDataRepository(tcd.mr.mongoClient)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, mr, tcd.mr)
			}
		})
	}
}

func TestMongoDataRepository_GetDataPoints(t *testing.T) {
	t.Parallel()

	testContext := getTestContext()
	testError := fmt.Errorf("some error")
	testStandard := getTestStandard(t)

	testCases := []struct {
		name        string
		mongoClient mongo.MongoClient
		ctx         context.Context
		standard    model.Standard
		hasError    bool
	}{
		{
			name:        "when standard is empty",
			mongoClient: getTestMongoClient(t, nil, nil, nil, nil),
			ctx:         testContext,
			standard:    model.Standard{},
			hasError:    true,
		},
		{
			name:        "when findOne fails",
			mongoClient: getTestMongoClient(t, nil, testError, nil, nil),
			ctx:         testContext,
			standard:    testStandard,
			hasError:    true,
		},
		{
			name:        "when all succeeds",
			mongoClient: getTestMongoClient(t, nil, nil, nil, nil),
			ctx:         testContext,
			standard:    testStandard,
			hasError:    false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		mr, err := NewMongoDataRepository(tcd.mongoClient)

		require.NoError(t, err)

		// TODO: how to assert the return data points?
		_, err = mr.GetDataPointRanges(tcd.ctx, tcd.standard)

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestMongoDataRepository_StoreStandard(t *testing.T) {
	t.Parallel()

	testError := fmt.Errorf("some error")
	testStandard := getTestStandard(t)
	testDataPointRanges := getTestDataPointRanges(t)

	testCases := []struct {
		name            string
		mongoClient     mongo.MongoClient
		standard        model.Standard
		dataPointRanges []model.DataPointRange
		hasError        bool
	}{
		{
			name:            "when standard is empty",
			mongoClient:     getTestMongoClient(t, nil, nil, nil, nil),
			standard:        model.Standard{},
			dataPointRanges: testDataPointRanges,
			hasError:        true,
		},
		{
			name:            "when deleting standard fails",
			mongoClient:     getTestMongoClient(t, nil, nil, nil, testError),
			standard:        testStandard,
			dataPointRanges: testDataPointRanges,
			hasError:        true,
		},
		{
			name:            "when inserting standard fails",
			mongoClient:     getTestMongoClient(t, nil, nil, testError, nil),
			standard:        testStandard,
			dataPointRanges: testDataPointRanges,
			hasError:        true,
		},
		{
			name:            "when all succeeds",
			mongoClient:     getTestMongoClient(t, nil, nil, nil, nil),
			standard:        testStandard,
			dataPointRanges: testDataPointRanges,
			hasError:        false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			mr, err := NewMongoDataRepository(tcd.mongoClient)

			require.NoError(t, err)

			err = mr.StoreStandard(context.Background(), tcd.standard, tcd.dataPointRanges)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func getTestMongoClient(
	t *testing.T,
	aggregateErr error,
	findOneErr error,
	insertOneErr error,
	deleteOneErr error,
) *mocks.MockMongoClient {
	ctrl := gomock.NewController(t)

	m := mocks.NewMockMongoClient(ctrl)

	m.
		EXPECT().
		// TODO: is it worth adding real parameters to this mock?
		Aggregate(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(aggregateErr).
		AnyTimes()

	m.
		EXPECT().
		// TODO: is it worth adding real parameters to this mock?
		FindOne(gomock.Any(), gomock.Any(), gomock.Any()).
		Return(findOneErr).
		AnyTimes()
	m.
		EXPECT().
		// TODO: is it worth adding real parameters to this mock?
		InsertOne(gomock.Any(), gomock.Any()).
		Return(gomock.Any().String(), insertOneErr).
		AnyTimes()
	m.
		EXPECT().
		// TODO: is it worth adding real parameters to this mock?
		DeleteOne(gomock.Any(), gomock.Any()).
		Return(deleteOneErr).
		AnyTimes()

	return m
}

func getTestDataPointRanges(t *testing.T) []model.DataPointRange {
	dpr, err := model.NewDataPointRange(
		model.AR_0_to_2,
		getTestDataPoints(),
	)

	require.NoError(t, err)

	return []model.DataPointRange{dpr}
}

func getTestContext() context.Context {
	return context.Background()
}
