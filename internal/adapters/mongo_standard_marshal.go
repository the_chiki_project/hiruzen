package adapters

import "github.com/theobitoproject/hiruzen/internal/domain/model"

func marshallDataPointRanges(dprs []model.DataPointRange) []DataPointRange {
	mdprs := []DataPointRange{}

	for _, dpr := range dprs {
		mdprs = append(mdprs, DataPointRange{
			AgeRange:   dpr.AgeRange().String(),
			DataPoints: marshallDataPoints(dpr.DataPoints()),
		})
	}

	return mdprs
}

func marshallDataPoints(dps []model.DataPoint) []DataPoint {
	mdps := []DataPoint{}

	for _, dp := range dps {
		mdps = append(mdps, DataPoint{
			Reference:              dp.Reference,
			BoxCoxPower:            dp.BoxCoxPower,
			Median:                 dp.Median,
			CoefficientOfVariation: dp.CoefficientOfVariation,
			Sd3Neg:                 dp.Sd3Neg,
			Sd2Neg:                 dp.Sd2Neg,
			Sd1Neg:                 dp.Sd1Neg,
			Sd0:                    dp.Sd0,
			Sd1Pos:                 dp.Sd1Pos,
			Sd2Pos:                 dp.Sd2Pos,
			Sd3Pos:                 dp.Sd3Pos,
		})
	}

	return mdps
}

func unmarshallDataPointRanges(mdps []DataPointRange) ([]model.DataPointRange, error) {
	dprs := []model.DataPointRange{}

	for _, mdp := range mdps {
		ageRange, err := model.NewAgeRange(mdp.AgeRange)
		if err != nil {
			return nil, err
		}

		dpr, err := model.NewDataPointRange(
			ageRange,
			unmarshallDataPoints(mdp.DataPoints),
		)

		if err != nil {
			return nil, err
		}

		dprs = append(dprs, dpr)
	}

	return dprs, nil
}

func unmarshallDataPoints(mdps []DataPoint) []model.DataPoint {
	dps := []model.DataPoint{}

	for _, mdp := range mdps {
		dps = append(dps, unmarshallDataPoint(mdp))
	}

	return dps
}

func unmarshallDataPoint(mdp DataPoint) model.DataPoint {
	return model.DataPoint{
		Reference:              mdp.Reference,
		BoxCoxPower:            mdp.BoxCoxPower,
		Median:                 mdp.Median,
		CoefficientOfVariation: mdp.CoefficientOfVariation,
		Sd3Neg:                 mdp.Sd3Neg,
		Sd2Neg:                 mdp.Sd2Neg,
		Sd1Neg:                 mdp.Sd1Neg,
		Sd0:                    mdp.Sd0,
		Sd1Pos:                 mdp.Sd1Pos,
		Sd2Pos:                 mdp.Sd2Pos,
		Sd3Pos:                 mdp.Sd3Pos,
	}
}
