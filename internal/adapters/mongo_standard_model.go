package adapters

// DataPoint is the mongo model for data point
type DataPoint struct {
	Reference              float32 `bson:"reference"`
	BoxCoxPower            float32 `bson:"boxcox_power"`
	Median                 float32 `bson:"median"`
	CoefficientOfVariation float32 `bson:"coefficient_of_variation"`

	Sd3Neg float32 `bson:"sd3Neg"`
	Sd2Neg float32 `bson:"sd2Neg"`
	Sd1Neg float32 `bson:"sd1Neg"`
	Sd0    float32 `bson:"sd0"`
	Sd1Pos float32 `bson:"sd1Pos"`
	Sd2Pos float32 `bson:"sd2Pos"`
	Sd3Pos float32 `bson:"sd3Pos"`
}

// DataPointRange is the mongo model for data point range
type DataPointRange struct {
	AgeRange   string      `bson:"age_range"`
	DataPoints []DataPoint `bson:"data_points"`
}

// MongoStandardModel is the mongo model for standard
type MongoStandardModel struct {
	Name                   string           `bson:"name"`
	Gender                 string           `bson:"gender"`
	DataPointReferenceName string           `bson:"data_point_reference_name"`
	DataPointRanges        []DataPointRange `bson:"data"`
}
