package adapters

import (
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func getTestStandard(t *testing.T) model.Standard {
	testStandard, err := model.NewStandard(
		model.WeightForLength,
		model.Boy,
	)

	require.NoError(t, err)

	return testStandard
}

func getTestDataPoints() []model.DataPoint {
	return []model.DataPoint{
		{
			Reference:              1,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
	}
}
