package app

import (
	"context"
	"fmt"
	"sync"

	"go.uber.org/multierr"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/zscore"
)

// Application is the abstraction of all actions that the service
// exposes for clients
type Application struct {
	fileParser         StandardFileParser
	filenameRepository StandardFilenameRepository
	dataRepository     StandardDataRepository
}

// NewApplication creates a new instance of Application
func NewApplication(
	fileParser StandardFileParser,
	filenameRepository StandardFilenameRepository,
	dataRepository StandardDataRepository,
) (Application, error) {
	if fileParser == nil {
		return Application{}, fmt.Errorf("file parser cannot be nil")
	}
	if filenameRepository == nil {
		return Application{}, fmt.Errorf("filename repository cannot be nil")
	}
	if dataRepository == nil {
		return Application{}, fmt.Errorf("data repository cannot be nil")
	}

	return Application{
		fileParser,
		filenameRepository,
		dataRepository,
	}, nil
}

// GetDataPointRanges returns the data for a single standard
func (app Application) GetDataPointRanges(
	ctx context.Context,
	standard model.Standard,
) ([]model.DataPointRange, error) {
	if standard.IsZero() {
		return []model.DataPointRange{}, fmt.Errorf("standard cannot be empty")
	}

	return app.dataRepository.GetDataPointRanges(ctx, standard)
}

// ImportAllStandards reads the information of all data files
// and store the information in the standard data store
func (app Application) ImportAllStandards(ctx context.Context) error {
	standards, err := model.GetAllStandards()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	wg.Add(len(standards))

	for _, standard := range standards {
		go func(standard model.Standard) {
			defer wg.Done()
			err = multierr.Append(err, app.ImportStandardData(ctx, standard))
		}(standard)
	}

	wg.Wait()

	return err
}

// ImportStandardData reads the information of data files
// related to a single standard
// and store the information in the standard data store
func (app Application) ImportStandardData(
	ctx context.Context,
	standard model.Standard,
) error {
	if standard.IsZero() {
		return fmt.Errorf("standard cannot be empty")
	}

	dfs, err := app.filenameRepository.GetDataFiles(standard)
	if err != nil {
		return err
	}

	dataPointRanges, err := app.fileParser.GetDataPoints(dfs)

	if err != nil {
		return err
	}

	return app.dataRepository.StoreStandard(ctx, standard, dataPointRanges)
}

// GetZScore computes Z score for all standard according to the
// child attributes
func (app Application) GetZScore(
	ctx context.Context,
	params zscore.ZScoreParameters,
) (zscore.ZScoreResults, float32, float32, error) {
	if params.IsZero() {
		return zscore.ZScoreResults{}, 0, 0, fmt.Errorf("z score parameters cannot be empty")
	}

	age := float32(params.AgeInMonths())
	BMI := params.BMI()

	results, err := zscore.NewZScoreResults()
	if err != nil {
		return zscore.ZScoreResults{}, 0, 0, err
	}

	getDataPointRangeForAgeRange := func(
		dprs []model.DataPointRange,
		age float32,
	) model.DataPointRange {
		for _, dpr := range dprs {
			if dpr.AgeRange().AgeIsInRange(age) {
				return dpr
			}
		}
		return model.DataPointRange{}
	}

	getZScoreForStandard := func(
		ctx context.Context,
		standard model.Standard,
		age float32,
		reference float32,
		measurement float32,
	) (zscore.ZScore, error) {
		dprs, err := app.dataRepository.GetDataPointRanges(ctx, standard)
		if err != nil {
			return zscore.ZScore{}, err
		}
		if len(dprs) <= 0 {
			return zscore.ZScore{}, nil
		}

		dpr := getDataPointRangeForAgeRange(dprs, age)
		if dpr.IsZero() {
			return zscore.ZScore{}, nil
		}

		dp := dpr.GetDataPointForReference(reference)

		// TODO: Is there a best way to handle this edge case?
		if dp.IsZero() {
			return zscore.ZScore{}, nil
		}

		score := zscore.GetZScoreValue(
			measurement,
			dp.BoxCoxPower,
			dp.Median,
			dp.CoefficientOfVariation,
		)

		return zscore.NewZScore(dpr.AgeRange(), reference, score)
	}

	computations := []computation{
		{
			standardName: model.BmiForAge,
			gender:       params.Gender(),
			age:          age,
			reference:    age,
			measurement:  BMI,
		},
		{
			standardName: model.LengthForAge,
			gender:       params.Gender(),
			age:          age,
			reference:    age,
			measurement:  params.AdjustedHeight(),
		},
		{
			standardName: model.WeightForAge,
			gender:       params.Gender(),
			age:          age,
			reference:    age,
			measurement:  params.Weight(),
		},
		{
			standardName: model.WeightForLength,
			gender:       params.Gender(),
			age:          age,
			reference:    params.AdjustedHeight(),
			measurement:  params.Weight(),
		},
	}

	var wg sync.WaitGroup
	wg.Add(len(computations))

	for _, comp := range computations {
		go func(comp computation) {
			defer wg.Done()
			standard, NewStandardErr := model.NewStandard(comp.standardName, comp.gender)
			if NewStandardErr != nil {
				err = multierr.Append(err, NewStandardErr)
				return
			}

			zScore, getZScoreForStandardErr := getZScoreForStandard(
				ctx,
				standard,
				comp.age,
				comp.reference,
				comp.measurement,
			)
			if getZScoreForStandardErr != nil {
				err = multierr.Append(err, getZScoreForStandardErr)
				return
			}

			results.SetZScore(comp.standardName, zScore)

		}(comp)
	}

	wg.Wait()

	return results, BMI, age, err
}

// IsZero defines if the instance is zero
func (app Application) IsZero() bool {
	return app == Application{}
}
