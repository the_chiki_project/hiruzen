package app

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/hiruzen/internal/app/mocks"
	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/stat"
	"github.com/theobitoproject/hiruzen/internal/domain/zscore"
)

func TestApp_NewApp(t *testing.T) {
	t.Parallel()

	testDataFiles := getTestDataFiles(t)
	testDataPointRanges := getTestDataPointRanges(t)
	testDataPoints := getTestDataPoints()

	testFileParser := getTestFileParser(
		t,
		testDataFiles,
		testDataPointRanges,
		nil,
	)
	testFilenameRepository := getTestFilenameRepository(
		t,
		getTestStandard(t),
		testDataFiles,
		nil,
	)
	testDataRepository := getTestDataRepository(
		t,
		getTestContext(),
		getTestStandard(t),
		12,
		testDataPointRanges,
		testDataPoints,
		nil,
	)

	testCases := []struct {
		name     string
		app      Application
		hasError bool
	}{
		{
			name: "when file parser is nil",
			app: Application{
				fileParser:         nil,
				filenameRepository: testFilenameRepository,
				dataRepository:     testDataRepository,
			},
			hasError: true,
		},
		{
			name: "when filename repository is nil",
			app: Application{
				fileParser:         testFileParser,
				filenameRepository: nil,
				dataRepository:     testDataRepository,
			},
			hasError: true,
		},
		{
			name: "when data repository is nil",
			app: Application{
				fileParser:         testFileParser,
				filenameRepository: testFilenameRepository,
				dataRepository:     nil,
			},
			hasError: true,
		},
		{
			name: "when all parameters are defined",
			app: Application{
				fileParser:         testFileParser,
				filenameRepository: testFilenameRepository,
				dataRepository:     testDataRepository,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			app, err := NewApplication(
				tcd.app.fileParser,
				tcd.app.filenameRepository,
				tcd.app.dataRepository,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, app, tcd.app)
			}
		})
	}
}

func TestApp_GetDataPointRanges(t *testing.T) {
	t.Parallel()

	testContext := getTestContext()
	testDataFiles := getTestDataFiles(t)
	testError := fmt.Errorf("some error")
	testDataPointRanges := getTestDataPointRanges(t)
	testDataPoints := getTestDataPoints()
	testStandard := getTestStandard(t)

	testFileParser := getTestFileParser(
		t,
		testDataFiles,
		testDataPointRanges,
		nil,
	)
	testFilenameRepository := getTestFilenameRepository(
		t,
		testStandard,
		testDataFiles,
		nil,
	)
	testDataRepository := getTestDataRepository(
		t,
		testContext,
		testStandard,
		12,
		testDataPointRanges,
		testDataPoints,
		nil,
	)

	testCases := []struct {
		name               string
		ctx                context.Context
		standard           model.Standard
		fileParser         StandardFileParser
		filenameRepository StandardFilenameRepository
		dataRepository     StandardDataRepository
		hasError           bool
	}{
		{
			name:               "when standard is empty",
			ctx:                testContext,
			standard:           model.Standard{},
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           true,
		},
		{
			name:               "when data repository fails",
			ctx:                testContext,
			standard:           model.Standard{},
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository: getTestDataRepository(
				t,
				testContext,
				testStandard,
				12,
				testDataPointRanges,
				testDataPoints,
				testError,
			),
			hasError: true,
		},
		{
			name:               "when all succeeds",
			ctx:                testContext,
			standard:           testStandard,
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		app, err := NewApplication(
			tcd.fileParser,
			tcd.filenameRepository,
			tcd.dataRepository,
		)

		require.NoError(t, err)

		dps, err := app.GetDataPointRanges(tcd.ctx, tcd.standard)

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, dps, testDataPointRanges)
			}
		})
	}
}

func TestApp_LoadStandardDataIntoDB(t *testing.T) {
	t.Parallel()

	testContext := getTestContext()
	testError := fmt.Errorf("some error")
	testDataFiles := getTestDataFiles(t)
	testDataPointRanges := getTestDataPointRanges(t)
	testDataPoints := getTestDataPoints()
	testStandard := getTestStandard(t)

	testFileParser := getTestFileParser(
		t,
		testDataFiles,
		testDataPointRanges,
		nil,
	)
	testFilenameRepository := getTestFilenameRepository(
		t,
		testStandard,
		testDataFiles,
		nil,
	)
	testDataRepository := getTestDataRepository(
		t,
		testContext,
		testStandard,
		12,
		testDataPointRanges,
		testDataPoints,
		nil,
	)

	testCases := []struct {
		name               string
		ctx                context.Context
		standard           model.Standard
		fileParser         StandardFileParser
		filenameRepository StandardFilenameRepository
		dataRepository     StandardDataRepository
		hasError           bool
	}{
		{
			name:               "when standard is empty",
			ctx:                testContext,
			standard:           model.Standard{},
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           true,
		},
		{
			name:       "when filename repository fails",
			ctx:        testContext,
			standard:   testStandard,
			fileParser: testFileParser,
			filenameRepository: getTestFilenameRepository(
				t,
				getTestStandard(t),
				testDataFiles,
				testError,
			),
			dataRepository: testDataRepository,
			hasError:       true,
		},
		{
			name:     "when file parser fails",
			ctx:      testContext,
			standard: testStandard,
			fileParser: getTestFileParser(
				t,
				testDataFiles,
				testDataPointRanges,
				testError,
			),
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           true,
		},
		{
			name:               "when data repository fails",
			ctx:                testContext,
			standard:           testStandard,
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository: getTestDataRepository(
				t,
				testContext,
				testStandard,
				12,
				testDataPointRanges,
				testDataPoints,
				testError,
			),
			hasError: true,
		},
		{
			name:               "when all succeeds",
			ctx:                testContext,
			standard:           testStandard,
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			app, err := NewApplication(
				tcd.fileParser,
				tcd.filenameRepository,
				tcd.dataRepository,
			)

			require.NoError(t, err)

			err = app.ImportStandardData(tcd.ctx, tcd.standard)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestApp_GetZScore(t *testing.T) {
	t.Parallel()

	testContext := getTestContext()
	testError := fmt.Errorf("some error")
	testDataFiles := getTestDataFiles(t)
	testDataPointRanges := getTestDataPointRanges(t)
	testDataPoints := getTestDataPoints()
	testZScoreParams := getTestZScoreParams(t)
	testStandard := getTestStandard(t)

	testFileParser := getTestFileParser(
		t,
		testDataFiles,
		testDataPointRanges,
		nil,
	)
	testFilenameRepository := getTestFilenameRepository(
		t,
		testStandard,
		testDataFiles,
		nil,
	)
	testDataRepository := getTestDataRepository(
		t,
		testContext,
		model.Standard{},
		0,
		testDataPointRanges,
		testDataPoints,
		nil,
	)

	testCases := []struct {
		name               string
		ctx                context.Context
		params             zscore.ZScoreParameters
		fileParser         StandardFileParser
		filenameRepository StandardFilenameRepository
		dataRepository     StandardDataRepository
		hasError           bool
	}{
		{
			name:               "when parameters is empty",
			ctx:                testContext,
			params:             zscore.ZScoreParameters{},
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           true,
		},
		{
			name:               "when get data point for reference fails",
			ctx:                testContext,
			params:             testZScoreParams,
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository: getTestDataRepository(
				t,
				testContext,
				model.Standard{},
				0,
				testDataPointRanges,
				testDataPoints,
				testError,
			),
			hasError: true,
		},
		// TODO: add cases for:
		// - when there is no data point ranges found
		// - when there is no data point found related to age
		{
			name:               "when all succeeds",
			ctx:                testContext,
			params:             testZScoreParams,
			fileParser:         testFileParser,
			filenameRepository: testFilenameRepository,
			dataRepository:     testDataRepository,
			hasError:           false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			app, err := NewApplication(
				tcd.fileParser,
				tcd.filenameRepository,
				tcd.dataRepository,
			)

			require.NoError(t, err)

			// TODO: assert z score results and BMI returned values
			_, _, _, err = app.GetZScore(tcd.ctx, tcd.params)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func getTestFileParser(
	t *testing.T,
	dataFiles []stat.DataFile,
	dataPointRanges []model.DataPointRange,
	err error,
) *mocks.MockStandardFileParser {
	ctrl := gomock.NewController(t)

	tfp := mocks.NewMockStandardFileParser(ctrl)
	tfp.
		EXPECT().
		GetDataPoints(dataFiles).
		Return(dataPointRanges, err).
		AnyTimes()

	return tfp
}

func getTestFilenameRepository(
	t *testing.T,
	standard model.Standard,
	dataFiles []stat.DataFile,
	err error,
) *mocks.MockStandardFilenameRepository {
	ctrl := gomock.NewController(t)

	tfr := mocks.NewMockStandardFilenameRepository(ctrl)
	tfr.
		EXPECT().
		GetDataFiles(standard).
		Return(dataFiles, err).
		AnyTimes()

	return tfr
}

func getTestDataRepository(
	t *testing.T,
	ctx context.Context,
	standard model.Standard,
	reference float32,
	dataPointRanges []model.DataPointRange,
	dataPoints []model.DataPoint,
	err error,
) *mocks.MockStandardDataRepository {
	ctrl := gomock.NewController(t)

	var mockStandard interface{}
	if standard.IsZero() {
		mockStandard = gomock.Any()
	} else {
		mockStandard = standard
	}

	tdr := mocks.NewMockStandardDataRepository(ctrl)
	tdr.
		EXPECT().
		GetDataPointRanges(ctx, mockStandard).
		Return(dataPointRanges, err).
		AnyTimes()

	tdr.
		EXPECT().
		StoreStandard(ctx, mockStandard, dataPointRanges).
		Return(err).
		AnyTimes()

	return tdr
}

func getTestStandard(t *testing.T) model.Standard {
	testStandard, err := model.NewStandard(
		model.WeightForLength,
		model.Boy,
	)

	require.NoError(t, err)

	return testStandard
}

func getTestDataFiles(t *testing.T) (dfs []stat.DataFile) {
	for i := 0; i < 3; i++ {
		df, err := stat.NewDataFile(
			"some directory",
			"some filename",
			"some sheet name",
			"some age range",
		)

		require.NoError(t, err)

		dfs = append(dfs, df)
	}

	return dfs
}

func getTestDataPointRanges(t *testing.T) []model.DataPointRange {
	dpr, err := model.NewDataPointRange(
		model.AR_0_to_2,
		getTestDataPoints(),
	)

	require.NoError(t, err)

	return []model.DataPointRange{dpr}
}

func getTestDataPoints() []model.DataPoint {
	return []model.DataPoint{
		{
			Reference:              1,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
	}
}

func getTestContext() context.Context {
	return context.Background()
}

func getTestZScoreParams(t *testing.T) zscore.ZScoreParameters {
	params, err := zscore.NewZScoreParameters(
		time.Now(),
		time.Now().AddDate(0, 2, 0),
		model.Boy,
		model.Recumbent,
		60,
		12,
	)

	require.NoError(t, err)

	return params
}
