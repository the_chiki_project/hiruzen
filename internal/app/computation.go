package app

import "github.com/theobitoproject/hiruzen/internal/domain/model"

type computation struct {
	standardName model.StandardName
	gender       model.Gender
	age          float32
	reference    float32
	measurement  float32
}
