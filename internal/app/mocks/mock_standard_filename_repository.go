// Code generated by MockGen. DO NOT EDIT.
// Source: internal/app/standard_filename_repository.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	model "github.com/theobitoproject/hiruzen/internal/domain/model"
	stat "github.com/theobitoproject/hiruzen/internal/domain/stat"
)

// MockStandardFilenameRepository is a mock of StandardFilenameRepository interface.
type MockStandardFilenameRepository struct {
	ctrl     *gomock.Controller
	recorder *MockStandardFilenameRepositoryMockRecorder
}

// MockStandardFilenameRepositoryMockRecorder is the mock recorder for MockStandardFilenameRepository.
type MockStandardFilenameRepositoryMockRecorder struct {
	mock *MockStandardFilenameRepository
}

// NewMockStandardFilenameRepository creates a new mock instance.
func NewMockStandardFilenameRepository(ctrl *gomock.Controller) *MockStandardFilenameRepository {
	mock := &MockStandardFilenameRepository{ctrl: ctrl}
	mock.recorder = &MockStandardFilenameRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockStandardFilenameRepository) EXPECT() *MockStandardFilenameRepositoryMockRecorder {
	return m.recorder
}

// GetDataFiles mocks base method.
func (m *MockStandardFilenameRepository) GetDataFiles(standard model.Standard) ([]stat.DataFile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetDataFiles", standard)
	ret0, _ := ret[0].([]stat.DataFile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetDataFiles indicates an expected call of GetDataFiles.
func (mr *MockStandardFilenameRepositoryMockRecorder) GetDataFiles(standard interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetDataFiles", reflect.TypeOf((*MockStandardFilenameRepository)(nil).GetDataFiles), standard)
}
