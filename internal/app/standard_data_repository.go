package app

import (
	"context"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

// StandardDataRepository is the handler to interact
// with the standard data store
type StandardDataRepository interface {
	// GetDataPointRanges returns the data for a single standard
	GetDataPointRanges(
		ctx context.Context,
		standard model.Standard,
	) ([]model.DataPointRange, error)
	// StoreStandard stores a standard together with their data and references
	StoreStandard(
		ctx context.Context,
		standard model.Standard,
		dataPointRanges []model.DataPointRange,
	) error
}
