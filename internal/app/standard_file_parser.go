package app

import (
	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/stat"
)

// StandardFileParser defines a handler to read data files
// and return the data from these files
type StandardFileParser interface {
	// GetDataPoints returns all data points and other data
	// related to the data files passed as parameter
	GetDataPoints(dataFiles []stat.DataFile) ([]model.DataPointRange, error)
}
