package app

import (
	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/stat"
)

// StandardFilenameRepository is the handler to interact
// with the standard filename data store
// This repository holds names, paths and sheet names about the data files
type StandardFilenameRepository interface {
	// GetDataFiles returns all data files related to a single standard
	GetDataFiles(standard model.Standard) ([]stat.DataFile, error)
}
