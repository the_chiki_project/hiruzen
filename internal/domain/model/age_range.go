package model

import (
	"fmt"
)

const (
	// TwoYearsInMonths defines the number of months in 2 years
	TwoYearsInMonths float32 = 24
	// FiveYearsInMonths defines the number of months in 5 years
	FiveYearsInMonths float32 = 60
)

var (
	// AR_0_to_2 defines the unique range reference name
	// for ages between 0 and 2 years
	AR_0_to_2 = AgeRange{"0_to_2"}
	// AR_2_to_5 defines the unique range reference name
	// for ages between 2 and 5 years
	AR_2_to_5 = AgeRange{"2_to_5"}
	// AR_0_to_5 defines the unique range reference name
	// for ages between 0 and 5 years
	AR_0_to_5 = AgeRange{"0_to_5"}
)

// AgeRangeValues defines all age ranges
var AgeRangeValues = []AgeRange{
	AR_0_to_2,
	AR_2_to_5,
	AR_0_to_5,
}

// AgeRange is the unique reference name
// for a specified range of ages in years
type AgeRange struct {
	name string
}

// NewAgeRange creates a new instance of AgeRange
func NewAgeRange(name string) (AgeRange, error) {
	for _, ageRange := range AgeRangeValues {
		if ageRange.String() == name {
			return ageRange, nil
		}
	}
	return AgeRange{}, fmt.Errorf("invalid %s age range", name)
}

// AgeIsInRange defines if a specified age number values
// is within range
func (ar AgeRange) AgeIsInRange(age float32) bool {
	if age > FiveYearsInMonths {
		return false
	}

	if ar == AR_0_to_5 {
		return true
	}

	if ar == AR_0_to_2 && age <= TwoYearsInMonths {
		return true
	}

	if ar == AR_2_to_5 && age > TwoYearsInMonths && age <= FiveYearsInMonths {
		return true
	}

	return false
}

// IsZero defines if the instance is zero
func (ar AgeRange) IsZero() bool {
	return ar == AgeRange{}
}

// String overrides the default String method
func (ar AgeRange) String() string {
	return ar.name
}
