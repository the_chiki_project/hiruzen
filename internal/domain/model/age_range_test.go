package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAgeRange_NewAgeRange(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		expected AgeRange
	}{
		{
			name:     AR_0_to_2.String(),
			expected: AR_0_to_2,
		},
		{
			name:     AR_2_to_5.String(),
			expected: AR_2_to_5,
		},
		{
			name:     AR_0_to_5.String(),
			expected: AR_0_to_5,
		},
		{
			name:     "",
			expected: AgeRange{},
		},
		{
			name:     "some invalid value",
			expected: AgeRange{},
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			ageRange, err := NewAgeRange(tcd.name)

			if tcd.expected.IsZero() {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tcd.expected, ageRange)
			}
		})
	}
}

func TestAgeRange_AgeIsInRange(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		ageRange AgeRange
		age      float32
		expected bool
	}{
		{
			name:     "when age is greater than 60",
			ageRange: AR_0_to_2,
			age:      78,
			expected: false,
		},
		{
			name:     "when age is lower than 60 and age range is 0_to_5",
			ageRange: AR_0_to_5,
			age:      50,
			expected: true,
		},
		{
			name:     "when age is between 24 and 60 and age range is 2_to_5",
			ageRange: AR_2_to_5,
			age:      50,
			expected: true,
		},
		{
			name:     "when age is lower than 24 and age range is 0_to_2",
			ageRange: AR_0_to_2,
			age:      18,
			expected: true,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			inRange := tcd.ageRange.AgeIsInRange(tcd.age)

			assert.Equal(t, inRange, tcd.expected)
		})
	}
}
