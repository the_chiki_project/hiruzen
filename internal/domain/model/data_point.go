package model

// DataPoint defines the structure of a single data point
type DataPoint struct {
	Reference              float32 // this could be related to age or length/height
	BoxCoxPower            float32 // also called "L"
	Median                 float32 // also called "M"
	CoefficientOfVariation float32 // alsso called "S"

	// Sd stands for "Standard Deviation"
	Sd3Neg float32
	Sd2Neg float32
	Sd1Neg float32
	Sd0    float32
	Sd1Pos float32
	Sd2Pos float32
	Sd3Pos float32
}

// IsZero defines if the instance is zero
func (dp DataPoint) IsZero() bool {
	return dp == DataPoint{}
}
