package model

import "fmt"

// DataPointRange holds all data points for a single age range
type DataPointRange struct {
	ageRange   AgeRange
	dataPoints []DataPoint
}

// NewDataPointRange creates a new instance of DataPointRange
func NewDataPointRange(
	ageRange AgeRange,
	dataPoints []DataPoint,
) (DataPointRange, error) {
	if ageRange.IsZero() {
		return DataPointRange{}, fmt.Errorf("age range cannot be empty")
	}

	return DataPointRange{
		ageRange,
		dataPoints,
	}, nil
}

// GetDataPointForReference returns the closest data point
// for a specified reference
func (dpr DataPointRange) GetDataPointForReference(reference float32) DataPoint {
	for i := 0; i < len(dpr.dataPoints); i++ {
		dp := dpr.dataPoints[i]

		if dp.Reference == reference {
			return dp
		}

		if i == 0 {
			continue
		}

		if dp.Reference > reference {
			return dpr.dataPoints[i-1]
		}
	}

	return DataPoint{}
}

// AgeRange is the getter for the age range value
func (dpr DataPointRange) AgeRange() AgeRange {
	return dpr.ageRange
}

// DataPoints is the getter for all data points
func (dpr DataPointRange) DataPoints() []DataPoint {
	return dpr.dataPoints
}

// IsZero defines if the instance is zero
func (dpr DataPointRange) IsZero() bool {
	return dpr.AgeRange().IsZero()
}
