package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDataPointRange_NewDataPointRange(t *testing.T) {
	t.Parallel()

	testDataPoints := getTestDataPoints()

	testCases := []struct {
		name           string
		dataPointRange DataPointRange
		hasError       bool
	}{
		{
			name: "when age range is empty",
			dataPointRange: DataPointRange{
				ageRange:   AgeRange{},
				dataPoints: testDataPoints,
			},
			hasError: true,
		},
		{
			name: "when all parameters are defined",
			dataPointRange: DataPointRange{
				ageRange:   AR_0_to_2,
				dataPoints: testDataPoints,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			dpr, err := NewDataPointRange(
				tcd.dataPointRange.ageRange,
				tcd.dataPointRange.dataPoints,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, dpr, tcd.dataPointRange)
			}
		})
	}
}

func TestDataPointRange_GetDataPointForReference(t *testing.T) {
	t.Parallel()

	testDataPoints := getTestDataPoints()

	testCases := []struct {
		name              string
		dataPointRange    DataPointRange
		reference         float32
		expectedDataPoint DataPoint
	}{
		{
			name: "when data points is empty",
			dataPointRange: DataPointRange{
				ageRange:   AR_0_to_2,
				dataPoints: []DataPoint{},
			},
			reference:         12,
			expectedDataPoint: DataPoint{},
		},
		{
			name: "when data points includes a reference with the same value as the parameter",
			dataPointRange: DataPointRange{
				ageRange:   AR_0_to_2,
				dataPoints: testDataPoints,
			},
			reference:         2,
			expectedDataPoint: testDataPoints[2],
		},
		{
			name: "when data points includes a reference with the closest value as the parameter",
			dataPointRange: DataPointRange{
				ageRange:   AR_0_to_2,
				dataPoints: testDataPoints,
			},
			reference:         3.5,
			expectedDataPoint: testDataPoints[3],
		},
		{
			name: "when data points does NOT found a data point",
			dataPointRange: DataPointRange{
				ageRange:   AR_0_to_2,
				dataPoints: testDataPoints,
			},
			reference:         12,
			expectedDataPoint: DataPoint{},
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			dp := tcd.dataPointRange.GetDataPointForReference(tcd.reference)

			assert.Equal(t, dp, tcd.expectedDataPoint)
		})
	}
}

func getTestDataPoints() []DataPoint {
	return []DataPoint{
		{
			Reference:              0,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
		{
			Reference:              1,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
		{
			Reference:              2,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
		{
			Reference:              3,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
		{
			Reference:              4,
			BoxCoxPower:            1.2,
			Median:                 1.2,
			CoefficientOfVariation: 1.2,
			Sd3Neg:                 1.1,
			Sd2Neg:                 1.1,
		},
	}
}
