package model

import (
	"fmt"
)

var (
	// Boy is the gender for boys
	Boy = Gender{"boy"}
	// Girl is the gender for girls
	Girl = Gender{"girl"}
)

// GenderValues defines all genders
var GenderValues = []Gender{
	Boy,
	Girl,
}

// Gender defines if a child is boy or girl
type Gender struct {
	name string
}

// NewGender creates an instance of Gender
func NewGender(name string) (Gender, error) {
	for _, gender := range GenderValues {
		if gender.String() == name {
			return gender, nil
		}
	}
	return Gender{}, fmt.Errorf("invalid %s gender", name)
}

// IsZero defines if the instance is zero
func (g Gender) IsZero() bool {
	return g == Gender{}
}

// String overrides the default String method
func (g Gender) String() string {
	return g.name
}
