package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGender_NewGender(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		expected Gender
	}{
		{
			name:     Boy.String(),
			expected: Boy,
		},
		{
			name:     Girl.String(),
			expected: Girl,
		},
		{
			name:     "",
			expected: Gender{},
		},
		{
			name:     "some invalid value",
			expected: Gender{},
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			gender, err := NewGender(tcd.name)

			if tcd.expected.IsZero() {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tcd.expected, gender)
			}
		})
	}
}
