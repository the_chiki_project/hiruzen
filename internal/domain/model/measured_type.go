package model

import (
	"fmt"
)

var (
	// Recumbent is the laydown measure
	Recumbent = MeasuredType{"recumbent"}
	// Standing is the standing measure
	Standing = MeasuredType{"standing"}
)

var measuredTypeValues = []MeasuredType{
	Recumbent,
	Standing,
}

// MeasuredType defines a possible way to take measures from a child
type MeasuredType struct {
	name string
}

// NewMeasuredType creates an instance of MeasuredType
func NewMeasuredType(name string) (MeasuredType, error) {
	for _, measuredType := range measuredTypeValues {
		if measuredType.String() == name {
			return measuredType, nil
		}
	}
	return MeasuredType{}, fmt.Errorf("invalid %s measured type", name)
}

// IsZero defines if the instance is zero
func (mt MeasuredType) IsZero() bool {
	return mt == MeasuredType{}
}

// String overrides the default String method
func (mt MeasuredType) String() string {
	return mt.name
}
