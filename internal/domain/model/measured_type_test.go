package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMeasuredType_NewMeasuredType(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		expected MeasuredType
	}{
		{
			name:     Recumbent.String(),
			expected: Recumbent,
		},
		{
			name:     Standing.String(),
			expected: Standing,
		},
		{
			name:     "",
			expected: MeasuredType{},
		},
		{
			name:     "some invalid value",
			expected: MeasuredType{},
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			measuredType, err := NewMeasuredType(tcd.name)

			if tcd.expected.IsZero() {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tcd.expected, measuredType)
			}
		})
	}
}
