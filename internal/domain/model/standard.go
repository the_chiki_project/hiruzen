package model

import (
	"fmt"
)

// Standard is the framework to define Z score
// for a specified standard name and gender
type Standard struct {
	name   StandardName
	gender Gender
}

// NewStandard creates a new instance of Standard
func NewStandard(name StandardName, gender Gender) (Standard, error) {
	if name.IsZero() {
		return Standard{}, fmt.Errorf("standard name cannot be empty")
	}
	if gender.IsZero() {
		return Standard{}, fmt.Errorf("gender cannot be empty")
	}
	return Standard{
		name,
		gender,
	}, nil
}

// GetAllStandards returns all available standards
func GetAllStandards() (standards []Standard, err error) {
	for _, g := range GenderValues {
		for _, n := range standardNameValues {
			s, err := NewStandard(n, g)
			if err != nil {
				return []Standard{}, err
			}

			standards = append(standards, s)
		}
	}

	return standards, nil
}

// Name is the getter for the standard name value
func (s Standard) Name() StandardName {
	return s.name
}

// Gender is the getter for the gender value
func (s Standard) Gender() Gender {
	return s.gender
}

// IsZero defines if the instance is zero
func (s Standard) IsZero() bool {
	return s.name.IsZero() || s.gender.IsZero()
}

// String overrides the default String method
func (s Standard) String() string {
	return fmt.Sprintf("%s - %s", s.name.String(), s.gender.String())
}
