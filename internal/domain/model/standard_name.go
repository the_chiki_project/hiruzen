package model

import "fmt"

var (
	age    string = "age"
	length string = "length"
)

var (
	// BmiForAge is the standard name for BMI vs Age
	BmiForAge = StandardName{name: "bmiForAge", reference: age}
	// LengthForAge is the standard name for Length/Height vs Age
	LengthForAge = StandardName{name: "lengthForAge", reference: age}
	// WeightForAge is the standard name for Weight vs Age
	WeightForAge = StandardName{name: "weightForAge", reference: age}
	// WeightForLength is the standard name for Weight vs Length/Height
	WeightForLength = StandardName{name: "weightForLength", reference: length}
)

var standardNameValues = []StandardName{
	BmiForAge,
	LengthForAge,
	WeightForAge,
	WeightForLength,
}

// StandardName defines the name of the standard defines by WHO
// and its reference to compare (age, length, etc)
type StandardName struct {
	name      string
	reference string
}

// NewStandardName creates a new instance of StandardName
func NewStandardName(name string) (StandardName, error) {
	for _, sn := range standardNameValues {
		if sn.String() == name {
			return sn, nil
		}
	}
	return StandardName{}, fmt.Errorf("invalid %s standard name", name)
}

// Reference is the getter for the reference value
func (sn StandardName) Reference() string {
	return sn.reference
}

// IsZero defines if the instance is zero
func (sn StandardName) IsZero() bool {
	return sn == StandardName{}
}

// String overrides the default String method
func (sn StandardName) String() string {
	return sn.name
}
