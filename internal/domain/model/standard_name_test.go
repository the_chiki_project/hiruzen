package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestStandardName_NewStandardName(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		expected StandardName
	}{
		{
			name:     BmiForAge.String(),
			expected: BmiForAge,
		},
		{
			name:     LengthForAge.String(),
			expected: LengthForAge,
		},
		{
			name:     WeightForAge.String(),
			expected: WeightForAge,
		},
		{
			name:     WeightForLength.String(),
			expected: WeightForLength,
		},
		{
			name:     "",
			expected: StandardName{},
		},
		{
			name:     "some invalid value",
			expected: StandardName{},
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			standardName, err := NewStandardName(tcd.name)

			if tcd.expected.IsZero() {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, standardName, tcd.expected)
			}
		})
	}
}
