package model

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestStandard_NewStandard(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		standard Standard
		hasError bool
	}{
		{
			name: "when standard name is empty",
			standard: Standard{
				name:   StandardName{},
				gender: Girl,
			},
			hasError: true,
		},
		{
			name: "when gender is empty",
			standard: Standard{
				name:   BmiForAge,
				gender: Gender{},
			},
			hasError: true,
		},
		{
			name: "when data is empty",
			standard: Standard{
				name:   BmiForAge,
				gender: Girl,
			},
			hasError: false,
		},
		{
			name: "when all parameters are defined",
			standard: Standard{
				name:   BmiForAge,
				gender: Girl,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			standard, err := NewStandard(
				tcd.standard.name,
				tcd.standard.gender,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, standard, tcd.standard)
			}
		})
	}
}

func TestStandard_GetAllStandards(t *testing.T) {
	t.Parallel()

	standards, err := GetAllStandards()

	require.NoError(t, err)

	assert.NotEqual(t, len(standards), 0)
}

func TestStandard_String(t *testing.T) {
	standard, err := NewStandard(
		WeightForLength,
		Boy,
	)

	require.NoError(t, err)

	assert.Equal(
		t,
		standard.String(),
		fmt.Sprintf("%s - %s", WeightForLength.String(), Boy.String()),
	)
}
