package stat

import "fmt"

// DataFile is the file that contains information about a single standard
type DataFile struct {
	parentDirectory string
	filename        string
	sheetName       string
	ageRange        string
}

// NewDataFile creates a new instance of DataFile
func NewDataFile(
	parentDirectory string,
	filename string,
	sheetName string,
	ageRange string,
) (DataFile, error) {
	if parentDirectory == "" {
		return DataFile{}, fmt.Errorf("parent directory cannot be empty")
	}
	if filename == "" {
		return DataFile{}, fmt.Errorf("filename cannot be empty")
	}
	if sheetName == "" {
		return DataFile{}, fmt.Errorf("sheetName cannot be empty")
	}
	if ageRange == "" {
		return DataFile{}, fmt.Errorf("ageRange cannot be empty")
	}

	return DataFile{
		parentDirectory,
		filename,
		sheetName,
		ageRange,
	}, nil
}

// ParentDirectory is the getter for the parent directory value
func (sdf DataFile) ParentDirectory() string {
	return sdf.parentDirectory
}

// Filename is the getter for the filename value
func (sdf DataFile) Filename() string {
	return sdf.filename
}

// SheetName is the getter for the sheet name value
func (sdf DataFile) SheetName() string {
	return sdf.sheetName
}

// IsZero defines if the instance is zero
func (sdf DataFile) IsZero() bool {
	return sdf == DataFile{}
}

// String overrides the default String method
func (sdf DataFile) String() string {
	return sdf.filename
}
