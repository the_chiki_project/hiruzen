package stat

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDataFile_NewDataFile(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		dataFile DataFile
		hasError bool
	}{
		{
			name: "when parent directory is empty",
			dataFile: DataFile{
				parentDirectory: "",
				filename:        "some filename",
				sheetName:       "some sheet name",
				ageRange:        "some age range",
			},
			hasError: true,
		},
		{
			name: "when filename is empty",
			dataFile: DataFile{
				parentDirectory: "some directory",
				filename:        "",
				sheetName:       "some sheet name",
				ageRange:        "some age range",
			},
			hasError: true,
		},
		{
			name: "when sheet name is empty",
			dataFile: DataFile{
				parentDirectory: "some directory",
				filename:        "some filename",
				sheetName:       "",
				ageRange:        "some age range",
			},
			hasError: true,
		},
		{
			name: "when age range is empty",
			dataFile: DataFile{
				parentDirectory: "some directory",
				filename:        "some filename",
				sheetName:       "some sheet name",
				ageRange:        "",
			},
			hasError: true,
		},
		{
			name: "when all parameters are defined",
			dataFile: DataFile{
				parentDirectory: "some directory",
				filename:        "some filename",
				sheetName:       "some sheet name",
				ageRange:        "some age range",
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			dataFile, err := NewDataFile(
				tcd.dataFile.parentDirectory,
				tcd.dataFile.filename,
				tcd.dataFile.sheetName,
				tcd.dataFile.ageRange,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, dataFile, tcd.dataFile)
			}
		})
	}
}
