package stat

import (
	"fmt"
	"path/filepath"
	"strconv"

	"github.com/theobitoproject/myoboku/excel"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

// FileParser reads a file that contains information about a single standard
// and returns this data
type FileParser struct {
	path   string
	reader excel.ExcelReader
}

// NewFileParser creates a new instance of FileParser
func NewFileParser(path string, reader excel.ExcelReader) (FileParser, error) {
	if path == "" {
		return FileParser{}, fmt.Errorf("path cannot be empty")
	}
	if reader == nil {
		return FileParser{}, fmt.Errorf("reader cannot be empty")
	}

	return FileParser{
		path,
		reader,
	}, nil
}

// GetDataPoints returns all data points and other data
// related to the data files passed as parameter
func (fp FileParser) GetDataPoints(
	dataFiles []DataFile,
) (dataPointRanges []model.DataPointRange, err error) {

	for _, df := range dataFiles {
		ageRange, err := model.NewAgeRange(df.ageRange)
		if err != nil {
			return []model.DataPointRange{}, err
		}

		dataPoints := []model.DataPoint{}

		fpath := filepath.Join(fp.path, df.ParentDirectory(), df.Filename())

		err = fp.reader.ForEachRow(
			fpath,
			df.SheetName(),
			func(row []string, index int) (bool, error) {
				if index == 0 {
					// skip header
					return true, nil
				}

				dp, dpErr := parseDataPoint(row)
				if dpErr != nil {
					return false, dpErr
				}

				dataPoints = append(dataPoints, dp)

				return true, nil
			})

		if err != nil {
			return []model.DataPointRange{}, err
		}

		dpr, err := model.NewDataPointRange(ageRange, dataPoints)
		if err != nil {
			return []model.DataPointRange{}, err
		}

		dataPointRanges = append(dataPointRanges, dpr)
	}

	return dataPointRanges, err
}

func parseDataPoint(row []string) (model.DataPoint, error) {

	flv, err := getFloatValues(row)
	if err != nil {
		return model.DataPoint{}, err
	}

	dp := model.DataPoint{
		Reference:              flv[0],
		BoxCoxPower:            flv[1],
		Median:                 flv[2],
		CoefficientOfVariation: flv[3],
		Sd3Neg:                 flv[4],
		Sd2Neg:                 flv[5],
		Sd1Neg:                 flv[6],
		Sd0:                    flv[7],
		Sd1Pos:                 flv[8],
		Sd2Pos:                 flv[9],
		Sd3Pos:                 flv[10],
	}

	return dp, nil
}

func getFloatValues(row []string) (flv []float32, err error) {

	for _, v := range row {
		fl, err := strconv.ParseFloat(v, 32)
		if err != nil {
			return nil, err
		}
		flv = append(flv, float32(fl))
	}

	return flv, nil
}
