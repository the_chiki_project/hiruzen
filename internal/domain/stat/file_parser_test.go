package stat

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/myoboku/excel"
	"github.com/theobitoproject/myoboku/excel/mocks"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func TestFileParser_NewFileParser(t *testing.T) {
	t.Parallel()

	testPath := "some path"

	testReader := getTestExcelReader(t, testPath, getTestDataFiles(t, ""), nil)

	testCases := []struct {
		name     string
		fp       FileParser
		hasError bool
	}{
		{
			name: "when path is empty",
			fp: FileParser{
				path:   "",
				reader: testReader,
			},
			hasError: true,
		},
		{
			name: "when reader is empty",
			fp: FileParser{
				path:   testPath,
				reader: nil,
			},
			hasError: true,
		},
		{
			name: "when all parameters are valid",
			fp: FileParser{
				path:   testPath,
				reader: testReader,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			fileParser, err := NewFileParser(
				tcd.fp.path,
				tcd.fp.reader,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, fileParser, tcd.fp)
			}
		})
	}
}

func TestFileParser_GetDataPoints(t *testing.T) {
	t.Parallel()

	testError := fmt.Errorf("some error")
	testPath := "some path"
	testDataFiles := getTestDataFiles(t, "")

	testCases := []struct {
		name     string
		path     string
		dfs      []DataFile
		reader   excel.ExcelReader
		hasError bool
	}{
		{
			name:     "when age range is invalid",
			path:     testPath,
			dfs:      getTestDataFiles(t, "some invalid age range name"),
			reader:   getTestExcelReader(t, testPath, testDataFiles, testError),
			hasError: true,
		},
		{
			name:     "when reader for each row fails",
			path:     testPath,
			dfs:      testDataFiles,
			reader:   getTestExcelReader(t, testPath, testDataFiles, testError),
			hasError: true,
		},
		// TODO: how to pass a function to the mock
		// in order to pass invalid row values and test error?

		// {
		// 	name:     "when data read from file includes invalid values",
		// 	path:     testPath,
		// 	df:       testDataFile,
		// 	reader:   getTestExcelReader(nil),
		// 	hasError: true,
		// },
		{
			name:     "when all succeeds",
			path:     testPath,
			dfs:      testDataFiles,
			reader:   getTestExcelReader(t, testPath, testDataFiles, nil),
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			fp, err := NewFileParser(tcd.path, tcd.reader)

			require.NoError(t, err)

			// TODO: assert data points returned in the first parameter
			_, err = fp.GetDataPoints(tcd.dfs)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func getTestExcelReader(
	t *testing.T,
	testPath string,
	testDataFiles []DataFile,
	err error,
) *mocks.MockExcelReader {
	ctrl := gomock.NewController(t)

	tr := mocks.NewMockExcelReader(ctrl)
	tr.
		EXPECT().
		ForEachRow(
			fmt.Sprintf(
				"%s/%s/%s",
				testPath,
				testDataFiles[0].ParentDirectory(),
				testDataFiles[0].Filename(),
			),
			testDataFiles[0].SheetName(),
			gomock.Any(),
		).
		Return(err).
		AnyTimes()

	return tr
}

func getTestDataFiles(t *testing.T, ageRange string) []DataFile {
	if ageRange == "" {
		ageRange = model.AR_0_to_2.String()
	}

	df, err := NewDataFile(
		"some directory",
		"some filename",
		"some sheet name",
		ageRange,
	)

	require.NoError(t, err)

	return []DataFile{
		df,
	}
}
