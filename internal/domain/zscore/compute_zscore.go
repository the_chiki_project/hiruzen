package zscore

import "math"

// GetZScoreValue computes the Z score value according to:
// X: physical measurement (e.g. weight, length, head circumference, stature or calculated BMI value)
// L: BoxCoxPower
// M: Median
// S: CoefficientOfVariation
func GetZScoreValue(
	X float32,
	L float32,
	M float32,
	S float32,
) float32 {
	if L == 0 {
		return getZScoreWhenLIsZero(X, M, S)
	} else {
		return getZScoreWhenLIsNotZero(X, L, M, S)
	}
}

func getZScoreWhenLIsZero(
	X float32,
	M float32,
	S float32,
) float32 {
	n := math.Log(float64(X / M))

	return float32(n) / S
}

func getZScoreWhenLIsNotZero(
	X float32,
	L float32,
	M float32,
	S float32,
) float32 {
	n := math.Pow(float64(X/M), float64(L)) - 1
	d := L * S

	return float32(n) / d
}
