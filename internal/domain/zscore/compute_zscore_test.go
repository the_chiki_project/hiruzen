package zscore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestZScore_GetZScore(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		X        float32
		L        float32
		M        float32
		S        float32
		expected float32
	}{
		{
			name:     "when L is zero",
			X:        7.8,
			L:        0,
			M:        6.4237,
			S:        0.12402,
			expected: 1.5653082,
		},
		{
			name:     "when L is NOT zero",
			X:        7.3,
			L:        -0.2024,
			M:        8.9481,
			S:        0.12268,
			expected: -1.6939899,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			zScore := GetZScoreValue(
				tcd.X,
				tcd.L,
				tcd.M,
				tcd.S,
			)

			assert.Equal(t, zScore, tcd.expected)
		})

	}
}
