package zscore

import (
	"fmt"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

// ZScore is the score that defines the state of a child's growth
type ZScore struct {
	ageRange  model.AgeRange
	reference float32
	value     float32
}

// NewZScore creates an instance of ZScore
func NewZScore(
	ageRange model.AgeRange,
	reference float32,
	value float32,
) (ZScore, error) {
	if ageRange.IsZero() {
		return ZScore{}, fmt.Errorf("age range cannot be empty")
	}

	return ZScore{
		ageRange,
		reference,
		value,
	}, nil
}

// SetValue is the setter for the z score value
func (zs *ZScore) SetValue(value float32) {
	zs.value = value
}

// AgeRange is the getter for the age range value
func (zs ZScore) AgeRange() model.AgeRange {
	return zs.ageRange
}

// Reference is the getter for the reference value
func (zs ZScore) Reference() float32 {
	return zs.reference
}

// Value is the getter for the z score value
func (zs ZScore) Value() float32 {
	return zs.value
}

// IsZero defines if the instance is zero
func (zs ZScore) IsZero() bool {
	return zs == ZScore{}
}
