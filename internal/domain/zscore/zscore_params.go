package zscore

import (
	"fmt"
	"math"
	"time"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

// standingMeasurementAdjustment is a coefficient that must be applied
// to the measured length/height in case the measured type is Standing
const standingMeasurementAdjustment = 0.7

// ZScoreParameters defines all the required parameters to
// calculate Z score
type ZScoreParameters struct {
	dateOfBirth  time.Time
	dateOfVisit  time.Time
	gender       model.Gender
	measuredType model.MeasuredType
	height       float32
	weight       float32
}

// NewZScoreParameters creates a new instance of ZScoreParameters
func NewZScoreParameters(
	dateOfBirth time.Time,
	dateOfVisit time.Time,
	gender model.Gender,
	measuredType model.MeasuredType,
	height float32,
	weight float32,
) (ZScoreParameters, error) {
	if dateOfBirth.IsZero() {
		return ZScoreParameters{}, fmt.Errorf("date of birth cannot be empty")
	}
	if dateOfVisit.IsZero() {
		return ZScoreParameters{}, fmt.Errorf("date of visit cannot be empty")
	}

	diff := dateOfVisit.Sub(dateOfBirth)
	if diff <= 0 {
		return ZScoreParameters{}, fmt.Errorf("date of visit must be greater than date of birth")
	}

	if gender.IsZero() {
		return ZScoreParameters{}, fmt.Errorf("gender cannot be empty")
	}
	if measuredType.IsZero() {
		return ZScoreParameters{}, fmt.Errorf("measured type cannot be empty")
	}
	if height == 0 {
		return ZScoreParameters{}, fmt.Errorf("height cannot be empty")
	}
	if weight == 0 {
		return ZScoreParameters{}, fmt.Errorf("weight cannot be empty")
	}

	return ZScoreParameters{
		dateOfBirth,
		dateOfVisit,
		gender,
		measuredType,
		height,
		weight,
	}, nil
}

// AgeInMonths returns the age represented in months
func (zsp ZScoreParameters) AgeInMonths() int {
	months := 0
	month := zsp.dateOfBirth.Month()
	for zsp.dateOfBirth.Before(zsp.dateOfVisit) {
		zsp.dateOfBirth = zsp.dateOfBirth.Add(time.Hour * 24)
		nextMonth := zsp.dateOfBirth.Month()
		if nextMonth != month {
			months++
		}
		month = nextMonth
	}

	return months
}

// BMI returns the computed body mass index
func (zsp ZScoreParameters) BMI() float32 {
	heightInMeters := zsp.AdjustedHeight() / 100
	return zsp.weight / (float32(math.Pow(float64(heightInMeters), 2)))
}

// AdjustedHeight returns the length/height with the adjustment
// only if the measured type is Standing
// If measured type is NOT Standing, length/height remains the same
func (zsp ZScoreParameters) AdjustedHeight() float32 {
	if zsp.measuredType == model.Standing {
		return zsp.height + standingMeasurementAdjustment
	}
	return zsp.height
}

// DateOfBirth is the getter for the date of birth value
func (zsp ZScoreParameters) DateOfBirth() time.Time {
	return zsp.dateOfBirth
}

// DateOfVisit is the getter for the date of visit value
func (zsp ZScoreParameters) DateOfVisit() time.Time {
	return zsp.dateOfVisit
}

// Gender is the getter for the gender value
func (zsp ZScoreParameters) Gender() model.Gender {
	return zsp.gender
}

// MeasuredType is the getter for the measured type value
func (zsp ZScoreParameters) MeasuredType() model.MeasuredType {
	return zsp.measuredType
}

// Height is the getter for the height value
func (zsp ZScoreParameters) Height() float32 {
	return zsp.height
}

// Weight is the getter for the weight value
func (zsp ZScoreParameters) Weight() float32 {
	return zsp.weight
}

// IsZero defines if the instance is zero
func (zsp ZScoreParameters) IsZero() bool {
	return zsp == ZScoreParameters{}
}
