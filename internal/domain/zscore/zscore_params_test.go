package zscore

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func TestZScoreParameters_NewZScoreParameters(t *testing.T) {
	t.Parallel()

	testDateOfBirth, testDateOfVisit := getTestDates(3)
	testGender := model.Boy
	testMeasuredType := model.Recumbent
	testHeight := float32(50)
	testWeight := float32(6)

	testCases := []struct {
		name         string
		zScoreParams ZScoreParameters
		hasError     bool
	}{
		{
			name: "when date of birth is empty",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  time.Time{},
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when date of visit is empty",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  time.Time{},
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when date of birth is equal or greater than date of visit",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfVisit.Add(time.Minute * 60),
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when gender is empty",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  testDateOfVisit,
				gender:       model.Gender{},
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when measured type is empty",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: model.MeasuredType{},
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when height is zero",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       0,
				weight:       testWeight,
			},
			hasError: true,
		},
		{
			name: "when weight is empty",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       0,
			},
			hasError: true,
		},
		{
			name: "when all parameters are defined",
			zScoreParams: ZScoreParameters{
				dateOfBirth:  testDateOfBirth,
				dateOfVisit:  testDateOfVisit,
				gender:       testGender,
				measuredType: testMeasuredType,
				height:       testHeight,
				weight:       testWeight,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			zScoreParams, err := NewZScoreParameters(
				tcd.zScoreParams.dateOfBirth,
				tcd.zScoreParams.dateOfVisit,
				tcd.zScoreParams.gender,
				tcd.zScoreParams.measuredType,
				tcd.zScoreParams.height,
				tcd.zScoreParams.weight,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, zScoreParams, tcd.zScoreParams)
			}
		})
	}
}

func TestZScoreParameters_AgeInMonths(t *testing.T) {
	t.Parallel()

	testMonths := 2

	testDateOfBirth, testDateOfVisit := getTestDates(testMonths)

	zScoreParams := ZScoreParameters{
		dateOfBirth:  testDateOfBirth,
		dateOfVisit:  testDateOfVisit,
		gender:       model.Boy,
		measuredType: model.Recumbent,
		height:       10,
		weight:       10,
	}

	assert.Equal(t, zScoreParams.AgeInMonths(), testMonths)
}

func TestZScoreParameters_BMI(t *testing.T) {
	t.Parallel()

	testDateOfBirth, testDateOfVisit := getTestDates(2)
	testGender := model.Boy

	testCases := []struct {
		name         string
		measuredType model.MeasuredType
		height       float32
		weight       float32
		expected     float32
	}{
		{
			name:         "when measured type is recumbent",
			measuredType: model.Recumbent,
			height:       174,
			weight:       84,
			expected:     27.744747,
		},
		{
			name:         "when measured type is standing",
			measuredType: model.Standing,
			height:       174,
			weight:       84,
			expected:     27.522856,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			zScoreParams, err := NewZScoreParameters(
				testDateOfBirth,
				testDateOfVisit,
				testGender,
				tcd.measuredType,
				tcd.height,
				tcd.weight,
			)

			require.NoError(t, err)

			assert.Equal(t, zScoreParams.BMI(), tcd.expected)
		})
	}
}

func TestZScoreParameters_AdjustedHeight(t *testing.T) {
	t.Parallel()

	testDateOfBirth, testDateOfVisit := getTestDates(2)
	testGender := model.Boy
	testWeight := float32(84)

	testCases := []struct {
		name         string
		measuredType model.MeasuredType
		height       float32
		expected     float32
	}{
		{
			name:         "when measured type is recumbent",
			measuredType: model.Recumbent,
			height:       174,
			expected:     174,
		},
		{
			name:         "when measured type is standing",
			measuredType: model.Standing,
			height:       174,
			expected:     174.7,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			zScoreParams, err := NewZScoreParameters(
				testDateOfBirth,
				testDateOfVisit,
				testGender,
				tcd.measuredType,
				tcd.height,
				testWeight,
			)

			require.NoError(t, err)

			assert.Equal(t, zScoreParams.AdjustedHeight(), tcd.expected)
		})
	}
}

// Returns test values for:
// - date of birth (first returned value)
// - date of visit (second returned value)
func getTestDates(diffInMonths int) (time.Time, time.Time) {
	return time.Now(), time.Now().AddDate(0, diffInMonths, 0)
}
