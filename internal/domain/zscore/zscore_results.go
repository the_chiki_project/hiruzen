package zscore

import "github.com/theobitoproject/hiruzen/internal/domain/model"

// ZScoreResults holds all Z score values for all standards
type ZScoreResults struct {
	bmiForAge       ZScore
	lengthForAge    ZScore
	weightForAge    ZScore
	weightForLength ZScore
}

// NewZScoreResults creates an new instance of ZScoreResults
func NewZScoreResults() (ZScoreResults, error) {
	return ZScoreResults{}, nil
}

// SetZScore sets the Z score value for a single standard
func (z *ZScoreResults) SetZScore(
	standardName model.StandardName,
	zScore ZScore,
) {
	switch standardName {

	case model.BmiForAge:
		z.setBmiForAge(zScore)

	case model.LengthForAge:
		z.setLengthForAge(zScore)

	case model.WeightForAge:
		z.setWeightForAge(zScore)

	case model.WeightForLength:
		z.setWeightForLength(zScore)

	}
}

func (z *ZScoreResults) setBmiForAge(zScore ZScore) {
	z.bmiForAge = zScore
}

func (z *ZScoreResults) setLengthForAge(zScore ZScore) {
	z.lengthForAge = zScore
}

func (z *ZScoreResults) setWeightForAge(zScore ZScore) {
	z.weightForAge = zScore
}

func (z *ZScoreResults) setWeightForLength(zScore ZScore) {
	z.weightForLength = zScore
}

// BmiForAge is the getter for the Z score value
// reñated to the BMI for age standard
func (z ZScoreResults) BmiForAge() ZScore {
	return z.bmiForAge
}

// LengthForAge is the getter for the Z score value
// reñated to the length/weight for age standard
func (z ZScoreResults) LengthForAge() ZScore {
	return z.lengthForAge
}

// WeightForAge is the getter for the Z score value
// reñated to the weight for age standard
func (z ZScoreResults) WeightForAge() ZScore {
	return z.weightForAge
}

// WeightForLength is the getter for the Z score value
// reñated to the weight for length standard
func (z ZScoreResults) WeightForLength() ZScore {
	return z.weightForLength
}
