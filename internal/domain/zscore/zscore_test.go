package zscore

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/theobitoproject/hiruzen/internal/domain/model"
)

func TestZScore_NewZScore(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		zScore   ZScore
		hasError bool
	}{
		{
			name: "when age range is empty",
			zScore: ZScore{
				ageRange:  model.AgeRange{},
				reference: 12,
				value:     12,
			},
			hasError: true,
		},
		{
			name: "when age range is NOT empty",
			zScore: ZScore{
				ageRange:  model.AR_0_to_2,
				reference: 12,
				value:     12,
			},
			hasError: false,
		},
	}

	for _, tc := range testCases {
		tcd := tc

		t.Run(tcd.name, func(t *testing.T) {
			t.Parallel()

			zs, err := NewZScore(
				tcd.zScore.ageRange,
				tcd.zScore.reference,
				tcd.zScore.value,
			)

			if tcd.hasError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, zs, tcd.zScore)
			}
		})
	}
}
