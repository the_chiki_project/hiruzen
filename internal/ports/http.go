package ports

import (
	"fmt"
	"net/http"
	"time"

	"github.com/theobitoproject/myoboku/httpsrv"
	"github.com/theobitoproject/myoboku/logger"

	"github.com/theobitoproject/hiruzen/internal/app"
	"github.com/theobitoproject/hiruzen/internal/domain/model"
	"github.com/theobitoproject/hiruzen/internal/domain/zscore"
)

// HttpServer defines the server that will expose the service for http requests
type HttpServer struct {
	application app.Application
	logger      logger.Logger
}

// NewHttpServer creates a new instance of HttpServer
func NewHttpServer(
	application app.Application,
	logger logger.Logger,
) (HttpServer, error) {
	if application.IsZero() {
		return HttpServer{}, fmt.Errorf("app cannot be empty")
	}
	if logger == nil {
		return HttpServer{}, fmt.Errorf("logger cannot be empty")
	}

	return HttpServer{
		application,
		logger,
	}, nil
}

// GetDataPointRanges returns the data for a single standard
func (hs HttpServer) GetDataPointRanges(
	w http.ResponseWriter,
	r *http.Request,
	params GetDataPointRangesParams,
) {
	standardName, err := model.NewStandardName(string(params.StandardName))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	gender, err := model.NewGender(string(params.Gender))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	standard, err := model.NewStandard(standardName, gender)
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	dprs, err := hs.application.GetDataPointRanges(r.Context(), standard)
	if err != nil {
		httpsrv.RespondInternalServerError(w, r, err)
		hs.logger.Error(err)
		return
	}

	mdprs := []DataPointRange{}

	for _, dpr := range dprs {
		mdps := []DataPoint{}

		for _, dp := range dpr.DataPoints() {
			mdps = append(mdps, DataPoint{
				Reference: dp.Reference,
				Sd3Neg:    dp.Sd3Neg,
				Sd2Neg:    dp.Sd2Neg,
				Sd1Neg:    dp.Sd1Neg,
				Sd0:       dp.Sd0,
				Sd1Pos:    dp.Sd1Pos,
				Sd2Pos:    dp.Sd2Pos,
				Sd3Pos:    dp.Sd3Pos,
			})
		}

		mdprs = append(mdprs, DataPointRange{
			AgeRange:   AgeRange(dpr.AgeRange().String()),
			DataPoints: mdps,
		})

	}

	httpsrv.RespondOK(w, r, GetDataPointRangesResponse{
		DataPointReferenceName: DataPointReferenceName(standard.Name().Reference()),
		DataPointRanges:        mdprs,
	})
}

// ImportAllStandards reads the information of all data files
// and store the information in the standard data store
func (hs HttpServer) ImportAllStandards(w http.ResponseWriter, r *http.Request) {
	err := hs.application.ImportAllStandards(r.Context())
	if err != nil {
		httpsrv.RespondInternalServerError(w, r, err)
		hs.logger.Error(err)
		return
	}

	httpsrv.RespondNoContent(w, r)
}

// ImportStandardData reads the information of data files
// related to a single standard
// and store the information in the standard data store
func (hs HttpServer) ImportSingleStandard(
	w http.ResponseWriter,
	r *http.Request,
	params ImportSingleStandardParams,
) {
	standardName, err := model.NewStandardName(string(params.StandardName))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	gender, err := model.NewGender(string(params.Gender))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	standard, err := model.NewStandard(standardName, gender)
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	err = hs.application.ImportStandardData(r.Context(), standard)
	if err != nil {
		httpsrv.RespondInternalServerError(w, r, err)
		hs.logger.Error(err)
		return
	}

	httpsrv.RespondNoContent(w, r)
}

// GetZScore computes Z score for all standard according to the
// child attributes
func (hs HttpServer) GetZScore(
	w http.ResponseWriter,
	r *http.Request,
	params GetZScoreParams,
) {
	gender, err := model.NewGender(string(params.Gender))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	measuredType, err := model.NewMeasuredType(string(params.MeasuredType))
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	zScoreParameters, err := zscore.NewZScoreParameters(
		time.Time(params.DateOfBirth.Time),
		time.Time(params.DateOfVisit),
		gender,
		measuredType,
		float32(params.Height),
		float32(params.Weight),
	)
	if err != nil {
		httpsrv.RespondBadRequest(w, r, err)
		return
	}

	zScoreResults, computedBMI, age, err := hs.application.GetZScore(
		r.Context(),
		zScoreParameters,
	)
	if err != nil {
		httpsrv.RespondInternalServerError(w, r, err)
		hs.logger.Error(err)
		return
	}

	httpsrv.RespondOK(w, r, GetZScoreResponse{
		BMI: BMI(computedBMI),
		Age: Age(age),
		ZScores: ZScoreResults{
			BmiForAge: ZScore{
				AgeRange:  AgeRange(zScoreResults.BmiForAge().AgeRange().String()),
				Reference: zScoreResults.BmiForAge().Reference(),
				Value:     zScoreResults.BmiForAge().Value(),
			},
			LengthForAge: ZScore{
				AgeRange:  AgeRange(zScoreResults.LengthForAge().AgeRange().String()),
				Reference: zScoreResults.LengthForAge().Reference(),
				Value:     zScoreResults.LengthForAge().Value(),
			},
			WeightForAge: ZScore{
				AgeRange:  AgeRange(zScoreResults.WeightForAge().AgeRange().String()),
				Reference: zScoreResults.WeightForAge().Reference(),
				Value:     zScoreResults.WeightForAge().Value(),
			},
			WeightForLength: ZScore{
				AgeRange:  AgeRange(zScoreResults.WeightForLength().AgeRange().String()),
				Reference: zScoreResults.WeightForLength().Reference(),
				Value:     zScoreResults.WeightForLength().Value(),
			},
		},
	})
}
