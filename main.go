package main

import (
	"fmt"
	"os"

	"github.com/go-chi/chi/v5"
	zscorePorts "github.com/theobitoproject/hiruzen/internal/ports"
	"github.com/theobitoproject/myoboku/enver"
	"github.com/theobitoproject/myoboku/httpsrv"
	"github.com/theobitoproject/myoboku/logger"
	"github.com/theobitoproject/myoboku/mongo"
)

func main() {
	fmt.Println("Hiruzen starting...")

	if len(os.Args) < 2 {
		panic("envPath is required as first argument when running main.go")
	}

	env, err := enver.NewGoDotEnver(os.Args[1])
	if err != nil {
		panic(err)
	}
	err = env.LoadEnv()
	if err != nil {
		panic(err)
	}

	logger, err := logger.NewDefaultLoggerHandler(
		logger.LoggerFormatType(env.Getenv(loggerType)),
		env.Getenv(loggerFile),
	)
	if err != nil {
		panic(err)
	}

	mongoConfig := mongo.NewDefaultMongoConfig(
		env.Getenv(mongoDbUrl),
		env.Getenv(mongoDbName),
	)

	zscoreServer, err := getZscoreHttpServer(env, logger, mongoConfig)
	if err != nil {
		panic(fmt.Sprintf("error creating zscore ports: %s", err))
	}

	mainServer := httpsrv.NewMainServer()

	mainServer.RegisterAsset(func(router chi.Router) {
		router.Use(httpsrv.GetCorsMiddleware(
			[]string{env.Getenv(allowedOrigins)},
			[]string{"GET", "POST", "OPTIONS"},
			[]string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
			false,
			300,
		))
	})

	mainServer.RegisterAsset(func(router chi.Router) {
		zscorePorts.HandlerFromMux(zscoreServer, router)
	})

	fmt.Println("Hiruzen served")

	err = mainServer.RunHttpServer(":" + env.Getenv(port))
	if err != nil {
		panic(fmt.Sprintf("error running http server: %s", err))
	}
}
