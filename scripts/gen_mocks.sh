#!/bin/bash

mockgen -package=mocks -source=internal/app/standard_data_repository.go -destination=internal/app/mocks/mock_standard_data_repository.go
mockgen -package=mocks -source=internal/app/standard_file_parser.go -destination=internal/app/mocks/mock_standard_file_parser.go
mockgen -package=mocks -source=internal/app/standard_filename_repository.go -destination=internal/app/mocks/mock_standard_filename_repository.go
