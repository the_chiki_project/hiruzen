#!/bin/bash

# hiruzen open API assets
oapi-codegen -generate types --old-config-style --package=ports -o internal/ports/openapi_types.gen.go api/openapi/hiruzen.yml
oapi-codegen -generate chi-server --old-config-style --package=ports -o internal/ports/openapi_api.gen.go api/openapi/hiruzen.yml
