package main

const (
	allowedOrigins           string = "ALLOWED_ORIGINS"
	port                     string = "PORT"
	loggerFile               string = "LOGGER_FILE"
	loggerType               string = "LOGGER_TYPE"
	mongoDbName              string = "MONGO_DB_NAME"
	mongoDbUrl               string = "MONGO_DB_URL"
	standardFilenameRepoFile string = "STANDARD_FILENAME_REPO_FILE"
	standardFilesPath        string = "STANDARD_FILES_PATH"
)
