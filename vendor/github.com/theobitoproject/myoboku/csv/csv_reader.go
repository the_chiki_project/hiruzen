package csv

// CsvReader defines a handler to read CSV files
type CsvReader interface {
	// ForEachRow allows to perform actions row per row
	ForEachRow(
		path string,
		closure func(row []string) (bool, error),
	) error
}
