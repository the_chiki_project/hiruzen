package csv

import (
	"fmt"

	"github.com/theobitoproject/myoboku/csv/file"
)

// DefaultCsvReader is a handler to read CSV files using default CSV golang package
type DefaultCsvReader struct {
	fs file.CsvFileSystem
}

// NewDefaultCsvReader creates a new instance of DefaultCsvReader
func NewDefaultCsvReader(fs file.CsvFileSystem) (CsvReader, error) {
	if fs == nil {
		return nil, fmt.Errorf("file system cannot be empty")
	}
	return &DefaultCsvReader{
		fs,
	}, nil
}

// ForEachRow allows to perform actions row per row
func (r *DefaultCsvReader) ForEachRow(
	path string,
	closure func(row []string) (bool, error),
) error {

	f, err := r.fs.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	rows, err := f.GetRows()
	if err != nil {
		return err
	}

	for _, row := range rows {
		keepLooping, err := closure(row)

		if err != nil {
			return err
		}

		if !keepLooping {
			break
		}
	}

	return nil
}
