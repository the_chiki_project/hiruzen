package file

import (
	"encoding/csv"
	"fmt"
	"os"
)

// DefaultCsvFile is the handler to manage action for a excel file
//
//	using default go package
type DefaultCsvFile struct {
	file *os.File
}

// NewDefaultCsvFile creates an instance of DefaultCsvFile
func NewDefaultCsvFile(file *os.File) (CsvFile, error) {
	if file == nil {
		return nil, fmt.Errorf("file cannot be empty")
	}
	return &DefaultCsvFile{
		file,
	}, nil
}

// GetRows returns all rows
func (cf *DefaultCsvFile) GetRows() ([][]string, error) {
	csvReader := csv.NewReader(cf.file)
	return csvReader.ReadAll()
}

// Close closes and cleanup the open temporary file
func (cf *DefaultCsvFile) Close() error {
	return cf.file.Close()
}
