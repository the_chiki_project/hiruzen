package file

import "os"

// DefaultCsvFileSystem is the handler of a csv file system
//
//	using default go packages
type DefaultCsvFileSystem struct{}

// NewDefaultCsvFileSystem creates an instance of DefaultCsvFileSystem
func NewDefaultCsvFileSystem() (CsvFileSystem, error) {
	return &DefaultCsvFileSystem{}, nil
}

// Open opens the csv file
func (fs *DefaultCsvFileSystem) Open(path string) (CsvFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return NewDefaultCsvFile(file)
}
