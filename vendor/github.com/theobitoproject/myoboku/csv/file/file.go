package file

// CsvFileSystem defines a system that contains csv files
//  and allows access to them
type CsvFileSystem interface {
	// Open opens the csv file
	Open(path string) (CsvFile, error)
}

// CsvFile defines the contract for perform action over an excel file
type CsvFile interface {
	// GetRows returns all rows
	GetRows() ([][]string, error)
	// Close closes and cleanup the open temporary file
	Close() error
}
