package enver

// Enver defines a way to query environment variables
type Enver interface {
	// LoadEnv loads environment variables from specific source
	LoadEnv() error
	// Getenv returns the value of an environment variable by its name
	Getenv(name string) string
}
