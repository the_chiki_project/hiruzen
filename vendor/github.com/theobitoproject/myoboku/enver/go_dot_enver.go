package enver

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

// GoDotEnver defines a enver handler using GoDotEnv library
type GoDotEnver struct {
	path string
}

// NewGoDotEnver creates a new instance of Enver
func NewGoDotEnver(path string) (Enver, error) {
	if path == "" {
		return nil, fmt.Errorf("path cannot be empty")
	}

	return &GoDotEnver{
		path,
	}, nil
}

// LoadEnv loads environment variables from specific source
func (e *GoDotEnver) LoadEnv() error {
	return godotenv.Load(e.path)
}

// Getenv returns the value of an environment variable by its name
func (e *GoDotEnver) Getenv(name string) string {
	return os.Getenv(name)
}
