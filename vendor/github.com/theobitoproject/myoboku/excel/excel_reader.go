package excel

// ExcelReader defines a handler to read excel files
type ExcelReader interface {
	// ForEachRow allows to perform actions row per row
	ForEachRow(
		path string,
		sheet string,
		closure func(row []string, index int) (bool, error),
	) error
}
