package excel

import (
	"fmt"

	"github.com/theobitoproject/myoboku/excel/file"
)

// ExcelizeReader is a handler to read excel files using excelize package
type ExcelizeReader struct {
	fs file.ExcelFileSystem
}

// NewExcelizeReader creates an instance of ExcelReader
func NewExcelizeReader(fs file.ExcelFileSystem) (ExcelReader, error) {
	if fs == nil {
		return nil, fmt.Errorf("file system cannot be empty")
	}
	return &ExcelizeReader{
		fs,
	}, nil
}

// ForEachRow allows to perform actions row per row
func (r *ExcelizeReader) ForEachRow(
	path string,
	sheet string,
	closure func(row []string, index int) (bool, error),
) error {

	f, err := r.fs.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	rows, err := f.GetRows(sheet)
	if err != nil {
		return err
	}

	for index, row := range rows {
		keepLooping, err := closure(row, index)

		if err != nil {
			return err
		}

		if !keepLooping {
			break
		}
	}

	return nil
}
