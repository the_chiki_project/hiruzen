package file

import (
	"fmt"

	"github.com/xuri/excelize/v2"
)

// ExcelizeFile is the handler to manage action for a excel file
//
//	using excelize package
type ExcelizeFile struct {
	file *excelize.File
}

// NewExcelizeFile creates an instance of ExcelFile
func NewExcelizeFile(file *excelize.File) (ExcelFile, error) {
	if file == nil {
		return nil, fmt.Errorf("file cannot be empty")
	}
	return &ExcelizeFile{
		file,
	}, nil
}

// GetRows returns all rows for a specific sheet
func (f *ExcelizeFile) GetRows(sheet string) ([][]string, error) {
	return f.file.GetRows(sheet)
}

// Close closes and cleanup the open temporary file
func (f *ExcelizeFile) Close() error {
	return f.file.Close()
}
