package file

import (
	"github.com/xuri/excelize/v2"
)

// ExcelizeFileSystem is the handler of a excel file system
//
//	using excelize package
type ExcelizeFileSystem struct{}

// NewExcelizeFileSystem creates an instance of ExcelFileSystem
func NewExcelizeFileSystem() (ExcelFileSystem, error) {
	return &ExcelizeFileSystem{}, nil
}

// Open opens the excel file
func (fs *ExcelizeFileSystem) Open(path string) (ExcelFile, error) {
	f, err := excelize.OpenFile(path)
	if err != nil {
		return nil, err
	}

	return NewExcelizeFile(f)
}
