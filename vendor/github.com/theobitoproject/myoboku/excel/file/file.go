package file

// ExcelFileSystem defines a system that contains excel files
//
//	and allows access to them
type ExcelFileSystem interface {
	// Open opens the excel file
	Open(path string) (ExcelFile, error)
}

// ExcelFile defines the contract for perform action over an excel file
type ExcelFile interface {
	// GetRows returns all rows for a specific sheet
	GetRows(sheet string) ([][]string, error)
	// Close closes and cleanup the open temporary file
	Close() error
}
