package httpsrv

import (
	"net/http"
)

// ErrorResponse defines the structure for a error response for a service
type ErrorResponse struct {
	Err        error `json:"-"`
	StatusCode int   `json:"-"`

	StatusText string `json:"status,omitempty"`
	ErrorText  string `json:"error,omitempty"`
}

// NewBadRequestError creates an instance of ErrorResponse with 400/Bad Request response
func NewBadRequestError(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:        err,
		StatusCode: http.StatusBadRequest,
		StatusText: "Bad Request",
		ErrorText:  err.Error(),
	}
}

// NewInternalServerError creates an instance of ErrorResponse with 500/Internal server error response
func NewInternalServerError(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:        err,
		StatusCode: http.StatusInternalServerError,
		StatusText: "Internal Server Error",
		ErrorText:  err.Error(),
	}
}

// Render renders a single payload and respond to the client request
func (er *ErrorResponse) Render(w http.ResponseWriter, r *http.Request) error {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(er.StatusCode)
	return nil
}
