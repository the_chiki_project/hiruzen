package httpsrv

import (
	"net/http"

	"github.com/go-chi/cors"
)

// GetCorsMiddleware allows enabling CORS with custom options
func GetCorsMiddleware(
	allowedOrigins []string,
	allowedMethods []string,
	allowedHeaders []string,
	allowCredentials bool,
	MaxAge int,
) func(next http.Handler) http.Handler {
	return cors.Handler(cors.Options{
		AllowedOrigins:   allowedOrigins,
		AllowedMethods:   allowedMethods,
		AllowedHeaders:   allowedHeaders,
		AllowCredentials: allowCredentials,
		MaxAge:           MaxAge,
	})
}
