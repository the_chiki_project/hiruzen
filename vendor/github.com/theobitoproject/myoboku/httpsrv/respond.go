package httpsrv

import (
	"net/http"

	"github.com/go-chi/render"
)

// Respond sends a response to the client
func Respond(w http.ResponseWriter, r *http.Request, v render.Renderer) {
	render.Render(w, r, v)
}

// START - SUCCESS

// RespondOK sends a 200/OK response to the client
func RespondOK(w http.ResponseWriter, r *http.Request, data interface{}) {
	Respond(w, r, NewOKSuccess(data))
}

// RespondOK sends a 204/No content response to the client
func RespondNoContent(w http.ResponseWriter, r *http.Request) {
	Respond(w, r, NewNoContentSuccess())
}

// END - SUCCESS

// START - CLIENT ERRORS

// RespondBadRequest sends a 400/Bad request response to the client
func RespondBadRequest(w http.ResponseWriter, r *http.Request, err error) {
	Respond(w, r, NewBadRequestError(err))
}

// END - CLIENT ERRORS

// START - SERVER ERRORS

// RespondInternalServerError sends a 500/Internal server error response to the client
func RespondInternalServerError(w http.ResponseWriter, r *http.Request, err error) {
	Respond(w, r, NewInternalServerError(err))
}

// END - SERVER ERRORS
