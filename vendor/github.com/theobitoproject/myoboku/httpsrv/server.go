package httpsrv

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

// MainServer is responsible for creating a HTTP services that will
//
//	receive requests and send responses
type MainServer struct {
	apiRouter chi.Router
}

// NewMainServer creates an instance of MainServer
func NewMainServer() *MainServer {
	apiRouter := chi.NewRouter()

	return &MainServer{
		apiRouter: apiRouter,
	}
}

// RegisterAsset allows clients to use the internal router to register
//
//	handlers, routes, middlewares, etc
func (ms *MainServer) RegisterAsset(
	register func(router chi.Router),
) {
	register(ms.apiRouter)
}

// RunHttpServer serves the http server and enables the service
func (ms *MainServer) RunHttpServer(
	addr string,
) error {
	rootRouter := chi.NewRouter()

	// TODO: do not hardcode "/api"
	rootRouter.Mount("/api", ms.apiRouter)

	return http.ListenAndServe(addr, rootRouter)
}
