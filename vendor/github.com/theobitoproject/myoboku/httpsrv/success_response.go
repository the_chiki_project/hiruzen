package httpsrv

import "net/http"

// SuccessResponse defines the structure for a success response for a service
type SuccessResponse struct {
	StatusCode int `json:"-"`

	StatusText string      `json:"status,omitempty"`
	Data       interface{} `json:"data,omitempty"`
}

// NewOKSuccess creates a instance of SuccessResponse with 200/OK response
func NewOKSuccess(Data interface{}) *SuccessResponse {
	return &SuccessResponse{
		StatusCode: http.StatusOK,
		StatusText: "OK",
		Data:       Data,
	}
}

// NewNoContentSuccess creates a instance of SuccessResponse with 204/No Content response
func NewNoContentSuccess() *SuccessResponse {
	return &SuccessResponse{
		StatusCode: http.StatusNoContent,
		StatusText: "No Content",
	}
}

// Render renders a single payload and respond to the client request
func (sr *SuccessResponse) Render(w http.ResponseWriter, r *http.Request) error {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(sr.StatusCode)
	return nil
}
