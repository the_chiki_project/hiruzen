package logger

// Logger defines a handler to log different type of errors
//
//	in different destinations
type Logger interface {
	// Error logs a message with error level
	Error(entry interface{})
	// Info logs a message with ingo level
	Info(entry interface{})
	// Warn logs a message with warning level
	Warn(entry interface{})
}
