package logger

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

// DefaultLoggerHandler defines the default and basic handler to log messages
type DefaultLoggerHandler struct {
	log *logrus.Logger
}

// NewDefaultLoggerHandler creates an instance of DefaultLoggerHandler
func NewDefaultLoggerHandler(
	loggerType LoggerFormatType,
	logFilePath string,
) (Logger, error) {
	if loggerType == "" {
		return nil, fmt.Errorf("logger type cannot be empty")
	}

	log := logrus.New()
	log.SetReportCaller(true)

	if loggerType == TextFormatter {
		log.SetFormatter(&logrus.TextFormatter{})

	} else if loggerType == JSONFormatter {
		log.SetFormatter(&logrus.JSONFormatter{})

	} else {
		return nil, fmt.Errorf("logger type must be text or JSON formatter")
	}

	if logFilePath != "" {
		file, err := os.OpenFile(logFilePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err == nil {
			log.Out = file
		} else {
			return nil, err
		}
	}

	return &DefaultLoggerHandler{
		log,
	}, nil
}

// Error logs a message with error level
func (l *DefaultLoggerHandler) Error(entry interface{}) {
	l.log.Error(entry)
}

// Info logs a message with ingo level
func (l *DefaultLoggerHandler) Info(entry interface{}) {
	l.log.Info(entry)
}

// Warn logs a message with warning level
func (l *DefaultLoggerHandler) Warn(entry interface{}) {
	l.log.Warn(entry)
}
