package logger

var (
	// TextFormatter is the text type format readable for humans
	TextFormatter LoggerFormatType = "TextFormatter"
	// TextFormatter is the JSON type format readable for humans and machines
	//  Useful when logs want to be shared in different systems
	JSONFormatter LoggerFormatType = "JSONFormatter"
)

// LoggerType defines the type of format of the log messages
type LoggerFormatType string
