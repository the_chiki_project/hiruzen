package mongo

import (
	"context"
	"fmt"

	"github.com/theobitoproject/myoboku/query"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	mongodriver "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DefaultMongoClient defines the default and basic handler to manage actions
//
//	with Mongo database
type DefaultMongoClient struct {
	mongoConfig MongoConfig
	collection  string
}

// NewDefaultMongoClient returns an instance of MongoClient
func NewDefaultMongoClient(
	mongoConfig MongoConfig,
	collection string,
) (MongoClient, error) {
	if mongoConfig == nil {
		return nil, fmt.Errorf("mongo config cannot be empty")
	}

	return &DefaultMongoClient{
		mongoConfig,
		collection,
	}, nil
}

// SetCollection sets the name of the collection
//
//	where actions will be performed
func (m *DefaultMongoClient) SetCollection(collection string) {
	m.collection = collection
}

// Aggregate executes an aggregate command
func (m *DefaultMongoClient) Aggregate(
	ctx context.Context,
	filters []interface{},
	decoder interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	pipeline := mongodriver.Pipeline{}

	for _, f := range filters {
		pipeline = append(pipeline, f.(bson.D))
	}

	cursor, err := collConn.Aggregate(ctx, pipeline)
	if err != nil {
		return err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		err = cursor.Decode(decoder)
		break
	}

	return err
}

// Find returns multiple records from the database
func (m *DefaultMongoClient) Find(
	ctx context.Context,
	queryBuilder query.QueryBuilder,
	decoder interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	opts := options.Find().
		SetProjection(parseFields(queryBuilder))

	cursor, err := collConn.Find(
		ctx,
		parseFilter(queryBuilder),
		opts,
	)
	if err != nil {
		return err
	}
	defer cursor.Close(ctx)

	return cursor.All(ctx, decoder)
}

// FindOne fetches a single record in database
func (m *DefaultMongoClient) FindOne(
	ctx context.Context,
	queryBuilder query.QueryBuilder,
	decoder interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	opts := options.FindOne().
		SetProjection(parseFields(queryBuilder))

	result := collConn.FindOne(
		ctx,
		parseFilter(queryBuilder),
		opts,
	)

	return result.Decode(decoder)
}

// InsertOne stores a single record in database
func (m *DefaultMongoClient) InsertOne(
	ctx context.Context,
	filter interface{},
) (string, error) {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return "", err
	}

	result, err := collConn.InsertOne(ctx, filter)
	if err != nil {
		return "", err
	}

	oid, isValid := result.InsertedID.(primitive.ObjectID)
	if !isValid {
		return "", err
	}

	return oid.Hex(), err
}

// UpdateOne updates a single record in database
func (m *DefaultMongoClient) UpdateOne(
	ctx context.Context,
	filter interface{},
	payload interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	_, err = collConn.UpdateOne(ctx, filter, payload)
	return err
}

// ReplaceOne replaces an entire record in database
func (m *DefaultMongoClient) ReplaceOne(
	ctx context.Context,
	queryBuilder query.QueryBuilder,
	payload interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	// TODO: could it be possible that upsert option
	// is not needed under the replace one context?
	opts := options.Replace().SetUpsert(true)

	_, err = collConn.ReplaceOne(
		ctx,
		parseFilter(queryBuilder),
		payload,
		opts,
	)
	return err
}

// DeleteOne deletes a single record in database
func (m *DefaultMongoClient) DeleteOne(
	ctx context.Context,
	filter interface{},
) error {
	collConn, err := m.getCollectionConn(ctx)
	if err != nil {
		return err
	}

	_, err = collConn.DeleteMany(ctx, filter)
	return err
}

// getCollectionConn returns the connection to a collection
func (m *DefaultMongoClient) getCollectionConn(
	ctx context.Context,
) (*mongodriver.Collection, error) {
	conn, err := m.getDBConn(ctx)
	if err != nil {
		return nil, err
	}

	return conn.Collection(m.collection), nil
}

// getDBConn returns the connection to a database
func (m *DefaultMongoClient) getDBConn(
	ctx context.Context,
) (*mongodriver.Database, error) {
	clientOptions := options.Client().ApplyURI(m.mongoConfig.GetConnUrl())
	client, err := mongodriver.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}

	return client.Database(m.mongoConfig.GetDBName()), nil
}
