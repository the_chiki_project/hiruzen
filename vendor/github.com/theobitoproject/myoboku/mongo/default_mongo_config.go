package mongo

// DefaultMongoConfig defines the default and basic configuration to manage actions
//
//	with Mongo database
type DefaultMongoConfig struct {
	connUrl string
	DBName  string
}

// NewDefaultMongoConfig creates an instance of MongoConfig
func NewDefaultMongoConfig(
	connUrl string,
	DBName string,
) MongoConfig {
	return &DefaultMongoConfig{
		connUrl,
		DBName,
	}
}

// GetConnUrl returns the URL of the database
func (mc DefaultMongoConfig) GetConnUrl() string {
	return mc.connUrl
}

// GetDBName returns the name of the DB
func (mc DefaultMongoConfig) GetDBName() string {
	return mc.DBName
}
