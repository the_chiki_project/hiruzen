package mongo

import (
	"context"

	"github.com/theobitoproject/myoboku/query"
)

// MongoClient defines a handler to manage actions with Mongo database
type MongoClient interface {
	// SetCollection sets the name of the collection
	//  where actions will be performed
	SetCollection(string)
	// Aggregate executes an aggregate command
	Aggregate(
		ctx context.Context,
		filters []interface{},
		decoder interface{},
	) error
	// Find returns multiple records from the database
	Find(
		ctx context.Context,
		queryBuilder query.QueryBuilder,
		decoder interface{},
	) error
	// FindOne fetches a single record in database
	FindOne(
		ctx context.Context,
		queryBuilder query.QueryBuilder,
		decoder interface{},
	) error
	// InsertOne stores a single record in database
	//  and returns the id of the stored record
	InsertOne(
		ctx context.Context,
		filter interface{},
	) (string, error)
	// UpdateOne updates a single record in database
	UpdateOne(
		ctx context.Context,
		filter interface{},
		payload interface{},
	) error
	// ReplaceOne replaces an entire record in database
	ReplaceOne(
		ctx context.Context,
		queryBuilder query.QueryBuilder,
		payload interface{},
	) error
	// DeleteOne deletes a single record in database
	DeleteOne(
		ctx context.Context,
		filter interface{},
	) error
}

// MongoConfig defines all data required to connect with a Mongo database
type MongoConfig interface {
	// GetConnUrl returns the URL of the database
	GetConnUrl() string
	// GetDBName returns the name of the DB
	GetDBName() string
}
