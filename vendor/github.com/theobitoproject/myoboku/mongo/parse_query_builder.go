package mongo

import (
	"github.com/theobitoproject/myoboku/query"
	"go.mongodb.org/mongo-driver/bson"
)

func parseFields(queryBuilder query.QueryBuilder) bson.M {
	fields := bson.M{}

	for _, f := range queryBuilder.GetFields() {
		fields[f] = 1
	}

	return fields
}

func parseFilter(queryBuilder query.QueryBuilder) bson.M {
	return bson.M(queryBuilder.GetFilters())
}
