package query

// DefaultQueryBuilder allows to create basic queries
// dynamically/programmatically
type DefaultQueryBuilder struct {
	fields  []string
	filters map[string]interface{}
}

// NewDefaultQueryBuilder creates a new instance of QueryBuilder
func NewDefaultQueryBuilder() QueryBuilder {
	return &DefaultQueryBuilder{
		fields:  []string{},
		filters: map[string]interface{}{},
	}
}

// GetFields returns the selected fields
func (qb *DefaultQueryBuilder) GetFields() []string {
	return qb.fields
}

// GetFilters returns all conditions to filter data
func (qb *DefaultQueryBuilder) GetFilters() map[string]interface{} {
	return qb.filters
}

// Where allows adding conditions to filter data
func (qb *DefaultQueryBuilder) Where(field string, value interface{}) {
	qb.filters[field] = value
}

// WithFields selects the specified fields
func (qb *DefaultQueryBuilder) WithFields(fields ...string) {
	for _, f := range fields {
		qb.fields = append(fields, f)
	}
}
