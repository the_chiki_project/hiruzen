package query

// QueryBuilder allows to create queries dynamically/programmatically
type QueryBuilder interface {
	// GetFields returns the selected fields
	GetFields() []string
	// GetFilters returns all conditions to filter data
	GetFilters() map[string]interface{}
	// Where allows adding conditions to filter data
	Where(string, interface{})
	// WithFields selects the specified fields
	WithFields(...string)
}
