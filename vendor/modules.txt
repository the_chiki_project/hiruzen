# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/deepmap/oapi-codegen v1.11.0
## explicit; go 1.16
github.com/deepmap/oapi-codegen/pkg/runtime
github.com/deepmap/oapi-codegen/pkg/types
# github.com/go-chi/chi/v5 v5.0.7
## explicit; go 1.14
github.com/go-chi/chi/v5
# github.com/go-chi/cors v1.2.1
## explicit; go 1.14
github.com/go-chi/cors
# github.com/go-chi/render v1.0.1
## explicit
github.com/go-chi/render
# github.com/go-stack/stack v1.8.0
## explicit
# github.com/golang/mock v1.6.0
## explicit; go 1.11
github.com/golang/mock/gomock
# github.com/golang/snappy v0.0.1
## explicit
github.com/golang/snappy
# github.com/google/uuid v1.3.0
## explicit
github.com/google/uuid
# github.com/joho/godotenv v1.4.0
## explicit; go 1.12
github.com/joho/godotenv
# github.com/klauspost/compress v1.13.6
## explicit; go 1.15
github.com/klauspost/compress
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/internal/snapref
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
## explicit
github.com/mohae/deepcopy
# github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe
## explicit
github.com/montanaflynn/stats
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/richardlehane/mscfb v1.0.4
## explicit
github.com/richardlehane/mscfb
# github.com/richardlehane/msoleps v1.0.1
## explicit
github.com/richardlehane/msoleps/types
# github.com/sirupsen/logrus v1.8.1
## explicit; go 1.13
github.com/sirupsen/logrus
# github.com/stretchr/testify v1.8.0
## explicit; go 1.13
github.com/stretchr/testify/assert
github.com/stretchr/testify/require
# github.com/theobitoproject/myoboku v1.20.0
## explicit; go 1.19
github.com/theobitoproject/myoboku/csv
github.com/theobitoproject/myoboku/csv/file
github.com/theobitoproject/myoboku/csv/mocks
github.com/theobitoproject/myoboku/enver
github.com/theobitoproject/myoboku/excel
github.com/theobitoproject/myoboku/excel/file
github.com/theobitoproject/myoboku/excel/mocks
github.com/theobitoproject/myoboku/httpsrv
github.com/theobitoproject/myoboku/logger
github.com/theobitoproject/myoboku/mongo
github.com/theobitoproject/myoboku/mongo/mocks
github.com/theobitoproject/myoboku/query
# github.com/xdg-go/pbkdf2 v1.0.0
## explicit; go 1.9
github.com/xdg-go/pbkdf2
# github.com/xdg-go/scram v1.1.1
## explicit; go 1.11
github.com/xdg-go/scram
# github.com/xdg-go/stringprep v1.0.3
## explicit; go 1.11
github.com/xdg-go/stringprep
# github.com/xuri/efp v0.0.0-20220407160117-ad0f7a785be8
## explicit; go 1.11
github.com/xuri/efp
# github.com/xuri/excelize/v2 v2.6.0
## explicit; go 1.15
github.com/xuri/excelize/v2
# github.com/xuri/nfp v0.0.0-20220409054826-5e722a1d9e22
## explicit; go 1.15
github.com/xuri/nfp
# github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d
## explicit
github.com/youmark/pkcs8
# go.mongodb.org/mongo-driver v1.10.1
## explicit; go 1.10
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/internal/randutil
go.mongodb.org/mongo-driver/internal/randutil/rand
go.mongodb.org/mongo-driver/internal/uuid
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/address
go.mongodb.org/mongo-driver/mongo/description
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/awsv4
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# go.uber.org/atomic v1.9.0
## explicit; go 1.13
go.uber.org/atomic
# go.uber.org/multierr v1.8.0
## explicit; go 1.14
go.uber.org/multierr
# golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
## explicit; go 1.17
golang.org/x/crypto/md4
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/ripemd160
# golang.org/x/net v0.0.0-20220513224357-95641704303c
## explicit; go 1.17
golang.org/x/net/html
golang.org/x/net/html/atom
golang.org/x/net/html/charset
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
## explicit
golang.org/x/sync/errgroup
# golang.org/x/sys v0.0.0-20220513210249-45d2b4557a2a
## explicit; go 1.17
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.7
## explicit; go 1.17
golang.org/x/text/encoding
golang.org/x/text/encoding/charmap
golang.org/x/text/encoding/htmlindex
golang.org/x/text/encoding/internal
golang.org/x/text/encoding/internal/identifier
golang.org/x/text/encoding/japanese
golang.org/x/text/encoding/korean
golang.org/x/text/encoding/simplifiedchinese
golang.org/x/text/encoding/traditionalchinese
golang.org/x/text/encoding/unicode
golang.org/x/text/feature/plural
golang.org/x/text/internal
golang.org/x/text/internal/catmsg
golang.org/x/text/internal/format
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/number
golang.org/x/text/internal/stringset
golang.org/x/text/internal/tag
golang.org/x/text/internal/utf8internal
golang.org/x/text/language
golang.org/x/text/message
golang.org/x/text/message/catalog
golang.org/x/text/runes
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
