package main

import (
	"github.com/theobitoproject/hiruzen/internal/adapters"
	"github.com/theobitoproject/hiruzen/internal/app"
	"github.com/theobitoproject/hiruzen/internal/domain/stat"
	"github.com/theobitoproject/hiruzen/internal/ports"
	"github.com/theobitoproject/myoboku/csv"
	csvFile "github.com/theobitoproject/myoboku/csv/file"
	"github.com/theobitoproject/myoboku/enver"
	"github.com/theobitoproject/myoboku/excel"
	excelFile "github.com/theobitoproject/myoboku/excel/file"
	"github.com/theobitoproject/myoboku/logger"
	"github.com/theobitoproject/myoboku/mongo"
)

func getZscoreHttpServer(
	env enver.Enver,
	logger logger.Logger,
	mongoConfig mongo.MongoConfig,
) (ports.HttpServer, error) {
	app, err := getZscoreApp(env, mongoConfig)
	if err != nil {
		return ports.HttpServer{}, err
	}
	return ports.NewHttpServer(app, logger)
}

func getZscoreApp(
	env enver.Enver,
	mongoConfig mongo.MongoConfig,
) (app.Application, error) {
	fp, err := getFileParser(env)
	if err != nil {
		return app.Application{}, err
	}

	fr, err := getFilenameRepository(env)
	if err != nil {
		return app.Application{}, err
	}

	dr, err := getDataRepository(mongoConfig)
	if err != nil {
		return app.Application{}, err
	}

	app, err := app.NewApplication(fp, fr, dr)
	if err != nil {
		return app, err
	}

	return app, nil
}

func getFileParser(env enver.Enver) (app.StandardFileParser, error) {
	fs, err := excelFile.NewExcelizeFileSystem()
	if err != nil {
		return stat.FileParser{}, err
	}

	reader, err := excel.NewExcelizeReader(fs)
	if err != nil {
		return stat.FileParser{}, err
	}

	fp, err := stat.NewFileParser(env.Getenv(standardFilesPath), reader)
	if err != nil {
		return stat.FileParser{}, err
	}

	return fp, err
}

func getFilenameRepository(
	env enver.Enver,
) (app.StandardFilenameRepository, error) {
	fs, err := csvFile.NewDefaultCsvFileSystem()
	if err != nil {
		return adapters.CsvFileRepository{}, err
	}

	reader, err := csv.NewDefaultCsvReader(fs)
	if err != nil {
		return adapters.CsvFileRepository{}, err
	}

	fr, err := adapters.NewCsvFileRepository(
		env.Getenv(standardFilenameRepoFile),
		reader,
	)

	return fr, err
}

func getDataRepository(mongoConfig mongo.MongoConfig) (app.StandardDataRepository, error) {

	m, err := mongo.NewDefaultMongoClient(mongoConfig, "standards")
	if err != nil {
		return adapters.MongoDataRepository{}, err
	}

	dr, err := adapters.NewMongoDataRepository(m)
	if err != nil {
		return adapters.MongoDataRepository{}, err
	}

	return dr, err
}
